Feature: [TC] [Login] Login successfully

	@TEST_ATL-989
	Scenario: [TC] [Login] Login successfully
		Given User navigate to login page
		When User input credentials data
		And User clicks "Login" button
		Then User is navigated to "Dashboard" page
