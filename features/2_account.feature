Feature: [TC] [Accounts] Create person account successfully

	@TEST_ATL-990
	Scenario: [TC] [Accounts] Create account successfully
		Given User navigates to Account list
		When User inputs valid new account data from csv file "./data/data_account.csv"
		Then System shows new account in the Account list "./data/data_account.csv"
