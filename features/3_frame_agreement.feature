Feature: [TC] [Frame Agreements] Create frame agreement for one phase product successfully

	@TEST_ATL-999
	Scenario: [TC] [Frame Agreements] Create frame agreement for one phase product successfully
		Given User opens Search and Filter form
		And User searches account from csv file "./data/data_open_account.csv"
		And User opens accounts from csv file "./data/data_open_account.csv"
		And User navigates to Frame Agreements List
		When User inputs valid frame agreement for one phase product from csv file "./data/data_frame_agreement.csv"
		Then System shows new record in the Frame Agreement in the list "./data/data_frame_agreement.csv"
		And System shows correct information in Frame Agreement details "./data/data_frame_agreement.csv"
		And System shows event in the History list
