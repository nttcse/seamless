import { Before, Then, When } from "@cucumber/cucumber";
import { AccountPage } from "../../page-objects/accountFunc";
import { logFailTestcase, logWarningMessage } from "../../shared/functions";
import { ICommonContext } from "../../shared/interfaces";
import { scenarioName } from "../../shared/variables";
import { GlobalPageObject } from "../../page-objects/global-page-object";

let accountPage: AccountPage;
let globalPageObject: GlobalPageObject;
const loader = require("csv-load-sync");

Before(async function () {
    const context: ICommonContext = this.context;
    accountPage = new AccountPage(context.driverService);
    globalPageObject = new GlobalPageObject(context.driverService);
});

When('User opens Search and Filter form', async () => {
    logFailTestcase(await accountPage.openSearchAndFilterForm(), "Open Search & Filter form failed!");
});

Then("User presses {string} button on Search & Filter form", async function (buttonName) {
    switch (buttonName) {
        case "Search": {
            logFailTestcase(await globalPageObject.pressSearchSearchAndFilter(), `Press "Search" Search and Filter form failed!`);
            break;
        }
        case "Save": {
            logFailTestcase(await globalPageObject.pressSaveSearchAndFilter(), `Press "Save" Search and Filter form failed!`);
            break;
        }
        case "Clear": {
            logFailTestcase(await globalPageObject.pressClearSearchAndFilter(), `Press "Clear" Search and Filter form failed!`);
            break;
        }
        case "Cancel": {
            logFailTestcase(await globalPageObject.pressCancelSearchAndFilter(), `Press "Cancel" Search and Filter form failed!`);
            break;
        }
        default: {
            logFailTestcase(false, `Button "${buttonName}" was not found on Search & Filter form!`);
            break;
        }
    }
});

// Filter Name form
When("User inputs Filter Name form {string}", async function (fileName) {
    const row = loader(fileName)[0];
    const FilterName = row.FilterName;
    const FilterType = row.FilterType;

    if (FilterName) {
        logFailTestcase(await globalPageObject.inputFilterNameFilterNameForm(FilterName), `Input filter name "${FilterName}" failed!`);
    }
    if (FilterType) {
        logFailTestcase(await globalPageObject.selectFilterTypeFilterNameForm(FilterType), `Select filter type "${FilterType}" failed!`);
    }
});

Then("User presses {string} button on Filter Name form", async function (buttonName) {
    await globalPageObject.waitForProgressBarLoaded_v2();
    switch (buttonName) {
        case "Cancel": {
            let temp = await globalPageObject.pressCancelForm();
            logFailTestcase(temp, `Press "${buttonName}" button on Filter Name form failed!`);
            break;
        }
        case "Save": {
            let temp = await globalPageObject.pressSaveForm();
            logFailTestcase(temp, `Press "${buttonName}" button on Filter Name form failed!`);
            break;
        }
        case "X": {
            await globalPageObject.closeOpeningForm();
            break;
        }
        default:
            logFailTestcase(false, `Press "${buttonName}" button on Filter Name form failed!`);
            break;
    }
});