import { Before, Then, When } from "@cucumber/cucumber";
import { fail } from "assert";
import { AccountPage } from "../../page-objects/accountFunc";
import { logFailTestcase, logWarningMessage } from "../../shared/functions";
import { ICommonContext } from "../../shared/interfaces";
import { scenarioName, subErrorMessages } from "../../shared/variables";
import { GlobalPageObject } from "../../page-objects/global-page-object";

let accountPage: AccountPage;
let globalPageObject: GlobalPageObject;
const loader = require("csv-load-sync");

Before(async function () {
    const context: ICommonContext = this.context;
    accountPage = new AccountPage(context.driverService);
    globalPageObject = new GlobalPageObject(context.driverService);
});

//Search and Filter
When("User searches account from csv file {string}", async (fileName) => {
    const rows = loader(fileName);
    for (let i = 0; i < rows.length; i++) {
        logWarningMessage(`Checking Search & Filter at line ${i + 1} in csv...`);
        //let temp = await accountPage.pressClearAtSearchAndFilter();
        //logFailTestcase(temp, "Press Clear at Search and Filter failed!");
        let temp;

        const Name = rows[i].Name;
        const OrgNo = rows[i].OrgNo;
        const Email = rows[i].Email;
        const Phone = rows[i].Phone;
        const KAM = rows[i].KAM;
        const Status = rows[i].Status;
        const Type = rows[i].Type;
        const Address = rows[i].Address;
        const Postcode = rows[i].Postcode;
        const City = rows[i].City;
        const Country = rows[i].Country;
        const QuoteReference = rows[i].QuoteReference;
        const PolicyReference = rows[i].PolicyReference;
        const PolicyStartDateFrom = rows[i].PolicyStartDateFrom;
        const PolicyStartDateTo = rows[i].PolicyStartDateTo;
        const PolicyEndDateFrom = rows[i].PolicyEndDateFrom;
        const PolicyEndDateTo = rows[i].PolicyEndDateTo;
        const Product = rows[i].Product;
        temp = await accountPage.inputValidDataToSearchAndFilterForm(
            Name,
            OrgNo,
            Email,
            Phone,
            KAM,
            Status,
            Type,
            Address,
            Postcode,
            City,
            Country,
            QuoteReference,
            PolicyReference,
            PolicyStartDateFrom,
            PolicyStartDateTo,
            PolicyEndDateFrom,
            PolicyEndDateTo,
            Product
        );
        logFailTestcase(temp, "Input data to Search & Filter form failed!");

        temp = await accountPage.pressSearchAtSearchAndFilter();
        logFailTestcase(temp, "Press Search at Search & Filter failed!");

        const AccountNumber = parseInt(rows[i].AccountNumber);
    }
});
Then('System shows a list of accounts at account list', async () => {
    //We have implemented at previous step for multiple searching account.
    console.info(scenarioName + ": Test case is passed!");
});

Then('System shows validation error messages on Search and Filter form', async () => {
    //We have implemented at previous step for multiple searching account.
    console.info(scenarioName + ": Test case is passed!");
});

Then('System shows new filter and filtered account list', async () => {
    //We have implemented at previous step for multiple searching account.
    console.info(scenarioName + ": Test case is passed!");
});