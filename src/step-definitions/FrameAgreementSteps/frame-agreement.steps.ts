import { Before, Given, Then, When } from "@cucumber/cucumber";
import { ICommonContext } from "../../shared/interfaces";
import { FrameAgreementPage } from "../../page-objects/frame-agreement-object";
import { GlobalPageObject } from "../../page-objects/global-page-object";
import { AccountPage } from "../../page-objects/accountFunc";
import { logWarningMessage, randomModulus11ForSSN, readJsonFileToObject, logFailTestcase, logSuccessMessage } from '../../shared/functions';
import { fail } from 'assert';
import { scenarioName, subErrorMessages } from '../../shared/variables';

const loader = require("csv-load-sync");
let frameAgreement: FrameAgreementPage;
let globalPageObject: GlobalPageObject;
//let accountPage: AccountPage;
let fileDataCreate: string = "";

//let expectedName: string;
//let expectedTotalLimitExposure: string;
let expectedFrameAgreementNo: string;

Before(async function () {
    const context: ICommonContext = this.context;
    frameAgreement = new FrameAgreementPage(context.driverService);
    globalPageObject = new GlobalPageObject(context.driverService);
});

Given("User navigates to Frame Agreements List", async () => {
    const temp = await frameAgreement.navigateToFrameAgreementsList();
    logFailTestcase(temp, "Navigate to Frame Agreements list failed!");
});

When(
    "User inputs valid frame agreement for one phase product from csv file {string}",
    async (filename: string) => {
        let rows = loader(filename);
        fileDataCreate = filename;

        for (const row of rows) {
            let temp = await frameAgreement.openCreateNewFrameAgreementForm();
            logFailTestcase(temp);
            const Name = row.Name;
            const TotalLimitExposure = row.TotalLimitExposure;
            const StartDate = row.StartDate;
            const EndDate = row.EndDate;
            const Product = row.Product;

            const Type = row.Type;
            const PremiumRate = row.PremiumRate;
            const CommissionRate = row.CommissionRate;
            const PaymentMethod = row.PaymentMethod;
            const GuaranteeRate = row.GuaranteeRate;
            const FirstPhaseGuaranteeRate = row.FirstPhaseGuaranteeRate;
            const SecondPhaseGuaranteeRate = row.SecondPhaseGuaranteeRate;
            const EstablishmentFee = row.EstablishmentFee;
            const AmendmentFee = row.AmendmentFee;

            temp = await frameAgreement.inputDataToCreateFrameAgreement(
                Name,
                TotalLimitExposure,
                StartDate,
                EndDate,
                Product
            );
            logFailTestcase(temp);

            temp = await frameAgreement.inputDataToAddProduct(
                Type,
                PremiumRate,
                CommissionRate,
                PaymentMethod,
                GuaranteeRate,
                FirstPhaseGuaranteeRate,
                SecondPhaseGuaranteeRate,
                EstablishmentFee,
                AmendmentFee,
            );
            logFailTestcase(temp);

            temp = await globalPageObject.pressSaveForm();
            logFailTestcase(temp);
        }
        await globalPageObject.pressSaveForm();
        await globalPageObject.waitForProgressBarLoaded();
    }
);

Then('System shows new record in the Frame Agreement in the list {string}', async (filename: string) => {
    const rows = loader(filename);
    let len = rows.length;
    await frameAgreement.reloadFrameAgreementList();
    //await globalPageObject.expandNumberOfItemMainList();

    for (let i = len - 1, j = 1; i >= 0; i--, j++) {
        const expectedName = rows[i].Name;
        const totalLimitExposure = rows[i].TotalLimitExposure;
        const expectedTotalLimitExposure = totalLimitExposure + ' NOK';

        const expectedStartDate = rows[i].StartDate;
        const expectedEndDate = rows[i].EndDate;
        const expectedCapacity = rows[i].Capacity;
        const expectedRemainingCapacity = totalLimitExposure + ' NOK';
        const expectedStatus = 'Active';

        await frameAgreement.assertCreateFrameAgreement(
            expectedName,
            expectedTotalLimitExposure,
            expectedStartDate,
            expectedEndDate,
            expectedCapacity,
            expectedRemainingCapacity,
            expectedStatus
        );
    }

    //expectedFrameAgreementNo = await frameAgreement.getFrameAgreementNo()
    //let expectedFrameAgreementNo: string;
});

Then('System shows correct information in Frame Agreement details {string}', async (filename: string) => {
    const rows = loader(filename);
    let len = rows.length;
    await frameAgreement.reloadFrameAgreementList();

    for (let i = len - 1, j = 1; i >= 0; i--, j++) {
        expectedFrameAgreementNo = await frameAgreement.getFrameAgreementNo()
        //const openframeagreement = rows[i].Name;
        await frameAgreement.openFrameAgreementDetails(expectedFrameAgreementNo);

        const expectedType = rows[i].Type;
        const expectedName = rows[i].Name;
        const expectedTotalLimitExposure = rows[i].TotalLimitExposure;
        const expectedStartDate = rows[i].StartDate;
        const expectedEndDate = rows[i].EndDate;
        const expectedRemainingCapacity = rows[i].TotalLimitExposure;
        const expectedProduct = rows[i].Product;
        const expectedPremiumRate = rows[i].PremiumRate;
        const expectedPaymentMethod = rows[i].PaymentMethod;
        const expectedGuaranteeRate = rows[i].GuaranteeRate;
        const expectedFirstPhaseGuaranteeRate = rows[i].FirstPhaseGuaranteeRate;
        const expectedSecondPhaseGuaranteeRate = rows[i].SecondPhaseGuaranteeRate;
        const expectedCommissionRate = rows[i].CommissionRate;
        const expectedEstablishmentFee = rows[i].EstablishmentFee;
        const expectedAmendmentFee = rows[i].AmendmentFee;

        await frameAgreement.assertFrameAgreementDetails(
            expectedType,
            expectedName,
            expectedTotalLimitExposure,
            expectedStartDate,
            expectedEndDate,
            expectedRemainingCapacity,
            expectedProduct,
            expectedPremiumRate,
            expectedCommissionRate,
            expectedPaymentMethod,
            expectedGuaranteeRate,
            expectedFirstPhaseGuaranteeRate,
            expectedSecondPhaseGuaranteeRate,
            expectedEstablishmentFee,
            expectedAmendmentFee
        )
    }
    await globalPageObject.pressCancelForm();
    expectedFrameAgreementNo = await frameAgreement.getFrameAgreementNo()
    //let expectedFrameAgreementNo: string;
});


Then("System shows event in the History list", async () => {
    const temp = await frameAgreement.openHistoryList();
    logFailTestcase(temp, "Navigate to History list failed!");

    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();

    const expectedType = 'Agreement created';
    const expectedDescription = `${expectedFrameAgreementNo} created`;
    const expectedTime = dd + '/' + mm + '/' + yyyy;

    await frameAgreement.assertHistory(
        expectedType,
        expectedDescription,
        expectedTime
    );
});
