import { Before, Given, Then, When } from "@cucumber/cucumber";
import { LoginPage } from "../../page-objects/login-loginFunc";
import { ICommonContext } from "../../shared/interfaces";
import { getLineInFileTxt, logFailTestcase } from "../../shared/functions";
import { GlobalPageObject } from "../../page-objects/global-page-object";
const loader = require("csv-load-sync");
let loginPage: LoginPage;
let globalPageObject: GlobalPageObject;
Before(async function () {
    const context: ICommonContext = this.context;
    loginPage = new LoginPage(context.driverService);
    globalPageObject = new GlobalPageObject(context.driverService);
});

Given("User navigate to login page", async function () {
    try {
        const url = getLineInFileTxt('./data/data_default.txt', 0).substring(4);
        await loginPage.navigate(url);
    } catch (error) {
        console.log(error);
    }
});

When("User input credentials data", async function () {
    try {
        const username = getLineInFileTxt('./data/data_default.txt', 1).substring(9);
        const passwordEncode = getLineInFileTxt('./data/data_default.txt', 2).substring(9);

        await loginPage.inputLogin(username, "username");
        await loginPage.inputLogin(passwordEncode, "password");
    } catch (error) {
        console.log(error);
    }
});

When("User clicks {string} button", async function (string) {
    if (string == "Login") {
        let temp = await loginPage.pressLogin();
        logFailTestcase(temp, "Press login button failed!");

        await globalPageObject.waitForProgressBarLoaded_v2();
    }
    // if (string == "Logout") {
    //   await globalPageObject.navigateToMainLogOut();
    // }
});

Then("User is navigated to {string} page", async function (string) {
    await loginPage.navigateInDashboard(string);
});