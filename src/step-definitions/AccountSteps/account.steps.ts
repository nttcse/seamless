import { Before, Given, Then, When } from "@cucumber/cucumber";
import { ICommonContext } from "../../shared/interfaces";
import { AccountPage } from "../../page-objects/accountFunc";
import { GlobalPageObject } from "../../page-objects/global-page-object";
import { logWarningMessage, randomModulus11ForSSN, readJsonFileToObject, logFailTestcase, logSuccessMessage } from '../../shared/functions';
import { fail } from 'assert';
import { scenarioName, subErrorMessages } from '../../shared/variables';

const loader = require("csv-load-sync");
let accountPage: AccountPage;
let globalPageObject: GlobalPageObject;
let fileDataCreate: string = "";
//Variable using to compare
let expectedName: string;
let expectedNIN: string;
let expectedEmail: string;
let expectedPhone: string;
let expectedAddress: string;
let expectedKAM: string;
let expectedStatus: string;

Before(async function () {
    const context: ICommonContext = this.context;
    accountPage = new AccountPage(context.driverService);
    globalPageObject = new GlobalPageObject(context.driverService);
});

Given("User navigates to Account list", async () => {
    const temp = await globalPageObject.navigateToMainAccountList();
    logFailTestcase(temp, "Navigate to Account list failed!");
});

When(
    "User inputs valid new account data from csv file {string}",
    async (filename: string) => {
        let rows = loader(filename);
        fileDataCreate = filename;
        for (const row of rows) {
            let temp = await accountPage.openCreateNewAccountForm();
            logFailTestcase(temp);
            const OrgNo = randomModulus11ForSSN(); //get random OrgNo with length = 11
            const CompanyName = row.CompanyName;
            const EmailAddress = row.EmailAddress;
            const CompanyPhone = row.CompanyPhone;
            const Status = row.Status;
            const Country = row.Country;

            const VisitingAddress = row.VisitingAddress;
            const VisitingExtraAddress = row.VisitingExtraAddress;
            const VisitingPostcode = row.VisitingPostcode;
            const VisitingCity = row.VisitingCity;

            const PostalAddress = row.PostalAddress;
            const PostalExtraAddress = row.PostalExtraAddress;
            const PostalPostcode = row.PostalPostcode;
            const PostalCity = row.PostalCity;

            const InvoiceAddress = row.InvoiceAddress;
            const InvoiceExtraAddress = row.InvoiceExtraAddress;
            const InvoicePostcode = row.InvoicePostcode;
            const InvoiceCity = row.InvoiceCity;

            const CreditScore = row.CreditScore;
            const CreditRating = row.CreditRating;
            const IndustryCode = row.IndustryCode;
            const Industry = row.Industry;
            const EducationalLevel = row.EducationalLevel;
            const CompanyRegistrationDate = row.CompanyRegistrationDate;
            const KAM = row.KAM;
            const PreferredCollectionDate = row.PreferredCollectionDate;
            const PaymentType = row.PaymentType;
            const PaymentFrequency = row.PaymentFrequency;

            expectedName = CompanyName;
            expectedNIN = OrgNo;
            expectedEmail = EmailAddress;
            expectedPhone = CompanyPhone;
            expectedAddress = VisitingAddress;
            let temp1 = ", " + PostalAddress;
            let temp2 = ", " + InvoiceAddress;
            if (temp1.localeCompare(", ") !== 0) {
                expectedAddress = expectedAddress + temp2;
            }
            if (temp2.localeCompare(", ") !== 0) {
                expectedAddress = expectedAddress + temp1;
            }

            expectedStatus = Status;
            temp = await accountPage.inputDataToCreateAccountForm(
                OrgNo,
                CompanyName,
                EmailAddress,
                CompanyPhone,
                Status,
                Country,
                VisitingAddress,
                VisitingExtraAddress,
                VisitingPostcode,
                VisitingCity,
                PostalAddress,
                PostalExtraAddress,
                PostalPostcode,
                PostalCity,
                InvoiceAddress,
                InvoiceExtraAddress,
                InvoicePostcode,
                InvoiceCity,
                CreditScore,
                CreditRating,
                IndustryCode,
                Industry,
                EducationalLevel,
                CompanyRegistrationDate,
                KAM,
                PreferredCollectionDate,
                PaymentType,
                PaymentFrequency
            );
            logFailTestcase(temp);

            temp = await globalPageObject.pressSaveForm();
            logFailTestcase(temp);

            temp = await globalPageObject.waitForProgressBarLoaded();
            logFailTestcase(temp);
        }
    }
);

Then("System shows new account in the Account list {string}", async (filename) => {
    const rows = loader(filename);
    let len = rows.length;
    await accountPage.reloadAccountList();

    for (let i = len - 1, j = 1; i >= 0; i--, j++) {
        const CompanyName = rows[i].CompanyName;
        const EmailAddress = rows[i].EmailAddress;
        const CompanyPhone = rows[i].CompanyPhone;
        const VisitingAddress = rows[i].VisitingAddress;
        const PostalAddress = rows[i].PostalAddress;
        const InvoiceAddress = rows[i].InvoiceAddress;
        const KAM = rows[i].KAM;
        const Status = rows[i].Status;

        expectedName = CompanyName;
        expectedEmail = EmailAddress;
        expectedPhone = CompanyPhone;
        expectedAddress = VisitingAddress;
        expectedKAM = KAM;
        expectedStatus = Status;

        let temp1 = ", " + PostalAddress;
        let temp2 = ", " + InvoiceAddress;
        if (temp1.localeCompare(", ") !== 0) {
            expectedAddress = expectedAddress + temp2;
        }
        if (temp2.localeCompare(", ") !== 0) {
            expectedAddress = expectedAddress + temp1;
        }

        await accountPage.assertCreateAccount(
            j, //position of row want to validate
            expectedName,
            expectedEmail,
            expectedPhone,
            expectedAddress,
            KAM,
            expectedStatus
        );
    }
    await accountPage.closeCreateAccountCompanyForm();
});

When("User opens accounts from csv file {string}", async (filename) => {
    try {
        const rows = loader(filename);
        fileDataCreate = filename;
        for (const row of rows) {

            const openedaccount = row.Name;
            await accountPage.clickAccountbtn();
            await accountPage.openDetailAccountByName(openedaccount);
            await accountPage.clickAccountbtn();
        }
    } catch (error) {
        console.log("User opened accounts from csv file");
        console.log(error);
    }
});

