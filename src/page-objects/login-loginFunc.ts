import { By } from "selenium-webdriver";
import { SeleniumWebDriverService } from "../core/selenium-webdriver.service";
import { Helpers } from "../shared/helpers";
import { waitUntilHorizontalProgressBarLoaded_v2 } from "../shared/functions";

export class LoginPage {
    private usernameField = By.xpath("//*[@placeholder='Enter username']");
    private passwordField = By.xpath("//*[@placeholder='Enter password']");

    private loginBtn = By.id("pgs-login-submit-button");
    private microsoftBtn = By.xpath("//button[@title='Use Microsoft Account']");

    private createNewDashboardBtn = By.xpath("//div/div[1]/div/button");
    private errorPath = By.xpath("");
    private MSIcon = By.xpath("//img[@alt='Microsoft']");
    private dashBoardBtn = By.id("dashboard-btn");
    private pageTitleDashboard = By.xpath("//*[contains(@class,'page-title') and contains(text(),'Dashboard')]");
    constructor(private driverService: SeleniumWebDriverService) { }

    public async navigate(data: string) {
        try {
            await this.driverService.goto(data);
            return true;
        } catch (error) {
            console.log("navigate");
            console.log(error);
            return false;
        }
    }

    public async inputLogin(data: string, type: string) {
        if (data == "not exist username" || data == "not exist password") {
            data = "abcdef";
        } else if (data == "empty") {
            data = "";
        }

        if (type == "username") {
            await this.driverService.setText(this.usernameField, data);
        } else if (type == "password") {
            const passwordDecode = Helpers.decode(data);
            // console.log('password encode:', data);
            // console.log('password decode:', passwordDecode);
            await this.driverService.setText(this.passwordField, passwordDecode);
        }
    }

    public async pressLogin() {
        try {
            await this.driverService.waitUntilElementLoaded(this.loginBtn);
            await waitUntilHorizontalProgressBarLoaded_v2(this.driverService, 10);
            await this.driverService.click(this.loginBtn);

            return true;
        } catch (error) {
            console.log("pressLogin");
            console.log(error);
            return false;
        }
    }

    public async navigateInDashboard(pageName: string) {
        await waitUntilHorizontalProgressBarLoaded_v2(this.driverService);
        await waitUntilHorizontalProgressBarLoaded_v2(this.driverService);
        await waitUntilHorizontalProgressBarLoaded_v2(this.driverService);
        await this.driverService.waitUntilElementLoaded(this.pageTitleDashboard);

        if (pageName == "Dashboard") {
            const result = await this.driverService.checkElementExist(
                this.createNewDashboardBtn
            );
            if (result != true) {
                throw new Error("Navigate wrong page !!**");
            }
        }
    }

    public async inputUsername(username: string) {
        try {
            await this.driverService.waitUntilElementLoaded(this.usernameField);
            await waitUntilHorizontalProgressBarLoaded_v2(this.driverService);
            await this.driverService.setText(this.usernameField, username);
            return true;
        } catch (error) {
            console.log("inputUsername");
            console.log(error);
            return false;
        }
    }

    public async inputPassword(passwordEncoded: string) {
        try {
            await this.driverService.waitUntilElementLoaded(this.passwordField);
            await waitUntilHorizontalProgressBarLoaded_v2(this.driverService, 10);
            await this.driverService.setText(this.passwordField, Helpers.decode(passwordEncoded));
            return true;
        } catch (error) {
            console.log("inputPassword");
            console.log(error);
            return false;
        }
    }
}
