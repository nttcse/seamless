import { By } from 'selenium-webdriver';
import { SeleniumWebDriverService } from '../core/selenium-webdriver.service';
import { closeToastMessage, expandNumberItemOfList, reloadTable, selectDropdownOption, waitUntilHorizontalProgressBarLoaded, waitUntilHorizontalProgressBarLoaded_v2 } from '../shared/functions';
import { locator_progressbarActive, toastError, toastSuccess } from '../shared/variables';

export class GlobalPageObject {
    constructor(private driverService: SeleniumWebDriverService) { }

    //Xpaths of elements at Header
    private dashboardBtnMenu = By.xpath("(//*[@id='navbarSupportedContent']//*[text()='Dashboard'])[1]");
    private accountsBtnMenu = By.xpath("(//*[@id='navbarSupportedContent']//*[text()='Accounts'])[1]");
    private saleAndDistributionBtnMenu = By.xpath("//*[@id='navbarSupportedContent']//li[not(@hidden)]//*[text()='Sales & Distribution']");
    private communicationsBtnMenu = By.xpath("(//*[@id='navbarSupportedContent']//*[text()='Communications'])[1]");
    private workflowsBtnMenu = By.xpath("(//*[@id='navbarSupportedContent']//*[text()='Workflows'])[1]");
    private insuranceBtnMenu = By.xpath("(//*[@id='navbarSupportedContent']//*[text()='Insurance'])[1]");
    private reportsBtnMenu = By.xpath("(//*[@id='navbarSupportedContent']//*[text()='Reports'])[1]");
    private claimsBtnMenu = By.xpath("(//*[@id='navbarSupportedContent']//*[text()='CLAIMS'])[1]");
    private entitiesBtnMenu = By.xpath("(//*[@id='navbarSupportedContent']//*[text()='Entities'])[1]");

    private notificationBtnMenu = By.xpath("//app-header-notification-list//*[@title='Notification']");
    private settingBtnMenu = By.xpath("(//app-header//i[contains(@class,'fa-cog')])[1]");

    private userProfileBtnMenu = By.xpath("(//app-header//*[contains(@class,'user-profile')])[1]");

    //Xpaths of elements as options of Header
    private accountsBtnOption = By.xpath("(//div[@class='dropdown']//span[text()='Accounts'])");
    private leadsBtnOption = By.xpath("(//div[@class='dropdown']//span[text()='Leads'])");
    private contactsBtnOption = By.xpath("(//div[@class='dropdown']//span[text()='Contacts'])");

    private salesBtnOption = By.xpath("(//div[@class='dropdown']//span[text()='Sales'])");
    private pipelineManagementBtnOption = By.xpath("(//div[@class='dropdown']//span[text()='Pipeline Management'])");

    private mailBtnOption = By.xpath("(//div[@class='dropdown']//span[text()='Mail'])");
    private callCenterBtnOption = By.xpath("(//div[@class='dropdown']//span[text()='Call Center'])");

    private caseManagementBtnOption = By.xpath("(//div[@class='dropdown']//span[text()='Case Management'])");
    private WorkflowSetupBtnOption = By.xpath("(//div[@class='dropdown']//span[text()='Workflow Setup'])");

    private productBuilderBtnOption = By.xpath("(//div[@class='dropdown']//span[text()='Product Builder'])");
    private documentTemplatesBtnOption = By.xpath("(//div[@class='dropdown']//span[text()='Document Templates'])");
    private quotesBtnOption = By.xpath("(//div[@class='dropdown']//span[text()='Quotes'])");

    private claimListBtnOption = By.xpath("(//div[@class='dropdown']//span[text()='Claim list'])");
    private FNOLBtnOption = By.xpath("(//div[@class='dropdown']//span[text()='FNOL'])");

    //xpaths of elements at Setting dropdown
    private usersBtnOptionSetting = By.xpath("//div[contains(@class,'dropdown-menu') and contains(@class,'show')]//button//span[text()='Users']");
    private rolesBtnOptionSetting = By.xpath("//div[contains(@class,'dropdown-menu') and contains(@class,'show')]//button//span[text()='Roles']");
    private moduleConfigurationBtnOptionSetting = By.xpath("//div[contains(@class,'dropdown-menu') and contains(@class,'show')]//button//span[text()='Module Configuration']");
    private entitiesBtnOptionSetting = By.xpath("//div[contains(@class,'dropdown-menu') and contains(@class,'show')]//button//span[text()='Entities']");
    private auditLogsBtnOptionSetting = By.xpath("//div[contains(@class,'dropdown-menu') and contains(@class,'show')]//button//span[text()='Audit Logs']");

    //xpaths of elements at Module Configuration
    private seamlessgrowBtn = By.xpath("//*[@id='seamless-grow}']");
    private saleanddistributionSettingBtn = By.xpath("//div[contains(@class,'domain-detail-item')][2]//button[contains(text(),'Settings')]");
    private seamlessflowBtn = By.xpath("//*[@id='seamless-flow}']");
    private workflowSettingBtn = By.xpath("//div[contains(@class,'domain-detail-item')][2]//button[contains(text(),'Settings')]");

    //xpaths of elements at User profile
    private myProfileBtnOptionUserProfile = By.xpath("//app-header//*[contains(@class,'user-profile') and contains(@class,'show')]//button[contains(text(),'My profile')]");
    private logOutBtnOptionUserProfile = By.xpath("//app-header//*[contains(@class,'user-profile') and contains(@class,'show')]//button[contains(text(),'Log out')]");


    //xpaths of elements at detail entities
    private summaryBtnTab = By.xpath("//div[contains(@class,'tab-pane') and contains(@class,'active')]//c-details-tab-layout//*[@id='pgs-acc-tab-Summary']");
    private quotesBtnTab = By.xpath("//div[contains(@class,'tab-pane') and contains(@class,'active')]//c-details-tab-layout//*[@id='pgs-acc-tab-Quotes']");
    private policiesBtnTab = By.xpath("//div[contains(@class,'tab-pane') and contains(@class,'active')]//c-details-tab-layout//*[@id='pgs-acc-tab-Policies']");
    private claimsBtnTab = By.xpath("//div[contains(@class,'tab-pane') and contains(@class,'active')]//c-details-tab-layout//*[@id='pgs-acc-tab-Claims']");
    private documentsBtnTab = By.xpath("//div[contains(@class,'tab-pane') and contains(@class,'active')]//c-details-tab-layout//*[@id='pgs-acc-tab-Documents']");
    private historyBtnTab = By.xpath("//div[contains(@class,'tab-pane') and contains(@class,'active')]//c-details-tab-layout//*[@id='pgs-acc-tab-History']");
    private casesBtnTab = By.xpath("//div[contains(@class,'tab-pane') and contains(@class,'active')]//c-details-tab-layout//*[@id='pgs-acc-tab-Cases']");
    private notesBtnTab = By.xpath("//div[contains(@class,'tab-pane') and contains(@class,'active')]//c-details-tab-layout//*[@id='pgs-acc-tab-Notes']");
    private smsBtnTab = By.xpath("//div[contains(@class,'tab-pane') and contains(@class,'active')]//c-details-tab-layout//*[@id='pgs-acc-tab-SMS']");
    private contactsBtnTab = By.xpath("//div[contains(@class,'tab-pane') and contains(@class,'active')]//c-details-tab-layout//*[@id='pgs-acc-tab-Contacts']");
    private entitiesBtnTab = By.xpath("//div[contains(@class,'tab-pane') and contains(@class,'active')]//c-details-tab-layout//*[@id='pgs-acc-tab-Entities']");
    private scoringBtnTab = By.xpath("//div[contains(@class,'tab-pane') and contains(@class,'active')]//c-details-tab-layout//*[@id='pgs-acc-tab-Scoring']");


    //Xpath of element on Search & Filter
    private btnSearchSearchAndFilter = By.xpath("//*[contains(local-name(),'form')]//div[contains(@class,'card-footer')]//*[text()='Search']");
    private btnSaveSearchAndFilter = By.xpath("//*[contains(local-name(),'form')]//div[contains(@class,'card-footer')]//*[text()='Save']");
    private btnClearSearchAndFilter = By.xpath("//*[contains(local-name(),'form')]//div[contains(@class,'card-footer')]//*[contains(text(),'Clear')]");
    private btnCancelSearchAndFilter = By.xpath("//*[contains(local-name(),'form')]//div[contains(@class,'card-footer')]//*[contains(text(),'Cancel')]");

    //Xpath of element on Filter Name form
    private txtFilterNameFilterNameForm = By.xpath("//app-c-filter-name-form//label[contains(text(),'Filter name')]/following-sibling::formly-input//input");
    private rdbGlobalFilterFilterNameForm = By.xpath("//app-c-filter-name-form//label[contains(text(),'Global Filter')]");
    private rdbPrivateFilterFilterNameForm = By.xpath("//app-c-filter-name-form//label[contains(text(),'Private Filter')]");

    //Xpath of element on form
    private btnSaveForm = By.xpath("(//*[contains(local-name(),'form')]//div[contains(@class,'modal-footer')]//*[contains(text(),'Save')])[last()]");
    private btnCancelForm = By.xpath("(//*[contains(local-name(),'form')]//div[contains(@class,'modal-footer')]//*[contains(text(),'Cancel')])[last()]");
    private btnYesForm = By.xpath("//*[contains(local-name(),'form')]//div[contains(@class,'modal-footer')]//*[contains(text(),'Yes')]");
    private btnNoForm = By.xpath("//*[contains(local-name(),'form')]//div[contains(@class,'modal-footer')]//*[contains(text(),'No')]");
    private btnRegisterForm = By.xpath("//*[contains(local-name(),'form')]//div[contains(@class,'modal-footer')]//*[contains(text(),'Register')]");

    //Xpath of element on Active tab
    private btnSaveTab = By.xpath("(//div[contains(@class,'tab-pane') and contains(@class,'active')]//div[contains(@class,'tab-pane') and contains(@class,'active')]//*[contains(text(),'Save')])[last()] | (//div[contains(@class,'tab-pane') and contains(@class,'active')]//*[contains(text(),'Save')])[last()]");

    // Xpath of element on Active sub tab
    private lblTotalNumberRecordSubTab = By.xpath("//div[contains(@class,'tab-pane') and contains(@class,'active')]//div[contains(@class,'tab-pane') and contains(@class,'active')]//div[contains(text(),'Total') and contains(text(),'records')]");


    // Xpath of element on Active main tab
    private lblTotalNumberRecordMainTab = By.xpath("//div[contains(@class,'tab-pane') and contains(@class,'active')]//div[contains(text(),'Total') and contains(text(),'records')]");

    //#region /* 1. Begin -- Navigate to Main List */
    public async navigateToMainDashBoard() {
        try {
            await this.driverService.waitUntilElementLoaded(this.dashboardBtnMenu);
            await waitUntilHorizontalProgressBarLoaded_v2(this.driverService, 500);
            await this.driverService.click(this.dashboardBtnMenu);
            return true;
        } catch (error) {
            console.log("navigateToMainDashBoard");
            console.log(error);
            return false;
        }
    }
    public async navigateToMainAccountList() {
        try {
            await this.driverService.waitUntilElementLoaded(this.accountsBtnMenu);
            await waitUntilHorizontalProgressBarLoaded_v2(this.driverService, 500);
            await this.driverService.click(this.accountsBtnMenu);
            await this.driverService.waitUntilElementLoaded(this.accountsBtnOption);
            await this.driverService.click(this.accountsBtnOption);
            return true;
        } catch (error) {
            console.log("navigateToMainAccountList");
            console.log(error);
            return false;
        }
    }

    public async navigateToMainLeadList() {
        try {
            await this.driverService.waitUntilElementLoaded(this.accountsBtnMenu);
            await waitUntilHorizontalProgressBarLoaded_v2(this.driverService, 500);
            await this.driverService.click(this.accountsBtnMenu);
            await this.driverService.waitUntilElementLoaded(this.leadsBtnOption);
            await this.driverService.click(this.leadsBtnOption);
            return true;
        } catch (error) {
            console.log("navigateToMainLeadList");
            console.log(error);
            return false;
        }
    }
    public async navigateToMainContactList() {
        try {
            await this.driverService.waitUntilElementLoaded(this.accountsBtnMenu);
            await waitUntilHorizontalProgressBarLoaded_v2(this.driverService, 500);
            await this.driverService.click(this.accountsBtnMenu);
            await this.driverService.waitUntilElementLoaded(this.contactsBtnOption);
            await this.driverService.click(this.contactsBtnOption);
            return true;
        } catch (error) {
            console.log("navigateToMainContactList");
            console.log(error);
            return false;
        }
    }

    private btnSaleListViewEnabled = By.xpath("(//div[contains(@class,'tab-pane') and contains(@class,'active')]//button[@id='pgs-slist-list-view-btn' and not(@disabled)])[last()]");
    private btnSaleListViewDisabled = By.xpath("//div[contains(@class,'tab-pane') and contains(@class,'active')]//button[@id='pgs-slist-list-view-btn' and (@disabled)]");
    public async navigateToMainSaleList() {
        try {
            await this.driverService.waitUntilElementLoaded(this.saleAndDistributionBtnMenu);
            await waitUntilHorizontalProgressBarLoaded_v2(this.driverService, 500);
            if (await this.driverService.isExisted(this.btnSaleListViewEnabled)) {
                await this.driverService.click(this.btnSaleListViewEnabled);
                await this.driverService.waitUntilElementLoaded(this.btnSaleListViewDisabled);
            }
            else {
                await this.driverService.waitUntilElementLoaded(this.saleAndDistributionBtnMenu);
                await this.driverService.click(this.saleAndDistributionBtnMenu);
                await this.driverService.waitUntilElementLoaded(this.salesBtnOption);
                await this.driverService.click(this.salesBtnOption);
            }
            await waitUntilHorizontalProgressBarLoaded_v2(this.driverService);
            return true;
        } catch (error) {
            console.log("navigateToMainSaleList");
            console.log(error);
            return false;
        }
    }
    public async navigateToMainPipelineManagement() {
        try {
            await this.driverService.waitUntilElementLoaded(this.settingBtnMenu);
            await waitUntilHorizontalProgressBarLoaded_v2(this.driverService, 500);
            await this.driverService.click(this.settingBtnMenu);
            await this.driverService.waitUntilElementLoaded(this.moduleConfigurationBtnOptionSetting);
            await this.driverService.click(this.moduleConfigurationBtnOptionSetting);
            await this.driverService.waitUntilElementLoaded(this.seamlessgrowBtn);
            await this.driverService.click(this.seamlessgrowBtn);
            await this.driverService.waitForElementEnabled(this.saleanddistributionSettingBtn);
            await this.driverService.click(this.saleanddistributionSettingBtn);
            return true;
        } catch (error) {
            console.log("navigateToMainPipelineManagement");
            console.log(error);
            return false;
        }
    }
    public async navigateToMainCaseManagementList() {
        try {
            await this.driverService.waitUntilElementLoaded(this.workflowsBtnMenu);
            await waitUntilHorizontalProgressBarLoaded_v2(this.driverService, 500);
            await this.driverService.click(this.workflowsBtnMenu);
            await this.driverService.waitUntilElementLoaded(this.caseManagementBtnOption);
            await this.driverService.click(this.caseManagementBtnOption);
            return true;
        } catch (error) {
            console.log("navigateToMainCaseManagementList");
            console.log(error);
            return false;
        }
    }
    public async navigateToMainWorkflowSetup() {
        try {
            await this.driverService.waitUntilElementLoaded(this.workflowsBtnMenu);
            await waitUntilHorizontalProgressBarLoaded_v2(this.driverService, 500);
            await this.driverService.click(this.workflowsBtnMenu);
            await this.driverService.waitUntilElementLoaded(this.WorkflowSetupBtnOption);
            await this.driverService.click(this.WorkflowSetupBtnOption);
            return true;
        } catch (error) {
            console.log("navigateToMainWorkflowSetup");
            console.log(error);
            return false;
        }
    }
    //Incase Workflow Setup move to Module Configuration -> Seamless Flow -> Workflow Setting
    /*public async navigateToMainWorkflowSetup() {
      try {
        await this.driverService.waitUntilElementLoaded(this.settingBtnMenu);
        await waitUntilHorizontalProgressBarLoaded_v2(this.driverService, 500);
        await this.driverService.click(this.settingBtnMenu);
        await this.driverService.waitUntilElementLoaded(this.moduleConfigurationBtnOptionSetting);
        await this.driverService.click(this.moduleConfigurationBtnOptionSetting);
        await this.driverService.waitUntilElementLoaded(this.seamlessflowBtn);
        await this.driverService.click(this.seamlessflowBtn);
        await this.driverService.waitForElementEnabled(this.workflowSettingBtn);
        await this.driverService.click(this.workflowSettingBtn);
        return true;
      } catch (error) {
        console.log("navigateToMainWorkflowSetup");
        console.log(error);
        return false;
      }
    }*/
    public async navigateToMainQuoteList() {
        try {
            await this.driverService.waitUntilElementLoaded(this.insuranceBtnMenu);
            await waitUntilHorizontalProgressBarLoaded_v2(this.driverService, 500);
            await this.driverService.click(this.insuranceBtnMenu);
            await this.driverService.waitUntilElementLoaded(this.quotesBtnOption);
            await this.driverService.click(this.quotesBtnOption);
            return true;
        } catch (error) {
            console.log("navigateToMainQuoteList");
            console.log(error);
            return false;
        }
    }
    public async navigateToMainClaimList() {
        try {
            await this.driverService.waitUntilElementLoaded(this.claimsBtnMenu);
            await waitUntilHorizontalProgressBarLoaded_v2(this.driverService, 500);
            await this.driverService.click(this.claimsBtnMenu);
            await this.driverService.waitUntilElementLoaded(this.claimListBtnOption);
            await this.driverService.click(this.claimListBtnOption);
            return true;
        } catch (error) {
            console.log("navigateToMainClaimList");
            console.log(error);
            return false;
        }
    }
    public async navigateToMainFNOL() {
        try {
            await this.driverService.waitUntilElementLoaded(this.claimsBtnMenu);
            await waitUntilHorizontalProgressBarLoaded_v2(this.driverService, 500);
            await this.driverService.click(this.claimsBtnMenu);
            await this.driverService.waitUntilElementLoaded(this.FNOLBtnOption);
            await this.driverService.click(this.FNOLBtnOption);
            return true;
        } catch (error) {
            console.log("navigateToMainFNOL");
            console.log(error);
            return false;
        }
    }
    public async navigateToMainUserList() {
        try {
            await this.driverService.waitUntilElementLoaded(this.settingBtnMenu);
            await waitUntilHorizontalProgressBarLoaded_v2(this.driverService, 500);
            await this.driverService.click(this.settingBtnMenu);
            await this.driverService.waitUntilElementLoaded(this.usersBtnOptionSetting);
            await this.driverService.click(this.usersBtnOptionSetting);
            return true;
        } catch (error) {
            console.log("navigateToMainUserList");
            console.log(error);
            return false;
        }
    }
    public async navigateToMainRoleList() {
        try {
            await this.driverService.waitUntilElementLoaded(this.settingBtnMenu);
            await waitUntilHorizontalProgressBarLoaded_v2(this.driverService, 500);
            await this.driverService.click(this.settingBtnMenu);
            await this.driverService.waitUntilElementLoaded(this.rolesBtnOptionSetting);
            await this.driverService.click(this.rolesBtnOptionSetting);
            return true;
        } catch (error) {
            console.log("navigateToMainRoleList");
            console.log(error);
            return false;
        }
    }
    public async navigateToMainModuleConfiguration() {
        try {
            await this.driverService.waitUntilElementLoaded(this.settingBtnMenu);
            await waitUntilHorizontalProgressBarLoaded_v2(this.driverService, 500);
            await this.driverService.click(this.settingBtnMenu);
            await this.driverService.waitUntilElementLoaded(this.moduleConfigurationBtnOptionSetting);
            await this.driverService.click(this.moduleConfigurationBtnOptionSetting);
            return true;
        } catch (error) {
            console.log("navigateToMainModuleConfiguration");
            console.log(error);
            return false;
        }
    }
    public async navigateToMainEntities() {
        try {
            await this.driverService.waitUntilElementLoaded(this.settingBtnMenu);
            await waitUntilHorizontalProgressBarLoaded_v2(this.driverService, 500);
            await this.driverService.click(this.settingBtnMenu);
            await this.driverService.waitUntilElementLoaded(this.entitiesBtnOptionSetting);
            await this.driverService.click(this.entitiesBtnOptionSetting);
            return true;
        } catch (error) {
            console.log("navigateToMainEntities");
            console.log(error);
            return false;
        }
    }
    public async navigateToMainAuditLog() {
        try {
            await this.driverService.waitUntilElementLoaded(this.settingBtnMenu);
            await waitUntilHorizontalProgressBarLoaded_v2(this.driverService, 500);
            await this.driverService.click(this.settingBtnMenu);
            await this.driverService.waitUntilElementLoaded(this.auditLogsBtnOptionSetting);
            await this.driverService.click(this.auditLogsBtnOptionSetting);
            return true;
        } catch (error) {
            console.log("navigateToMainAuditLog");
            console.log(error);
            return false;
        }
    }
    public async navigateToMainLogOut() {
        try {
            await this.driverService.waitUntilElementLoaded(this.userProfileBtnMenu);
            await waitUntilHorizontalProgressBarLoaded_v2(this.driverService, 500);
            await this.driverService.click(this.userProfileBtnMenu);
            await this.driverService.waitUntilElementLoaded(this.logOutBtnOptionUserProfile);
            await this.driverService.click(this.logOutBtnOptionUserProfile);
            return true;
        } catch (error) {
            console.log("navigateToMainLogOut");
            console.log(error);
            return false;
        }
    }
    //#endregion /* 1. End -- Navigate to Main List */

    //#region /*2. Begin -- Navigate to Sub tab*/
    public async navigateToSubSummary() {
        try {
            await this.driverService.waitUntilElementLoaded(this.summaryBtnTab);
            await waitUntilHorizontalProgressBarLoaded_v2(this.driverService, 500);
            await this.driverService.click(this.summaryBtnTab);
            return true;
        } catch (error) {
            console.log("navigateToSubSummary");
            console.log(error);
            return false;
        }
    }
    public async navigateToSubQuotes() {
        try {
            await this.driverService.waitUntilElementLoaded(this.quotesBtnTab);
            await waitUntilHorizontalProgressBarLoaded_v2(this.driverService, 500);
            await this.driverService.click(this.quotesBtnTab);
            return true;
        } catch (error) {
            console.log("navigateToSubQuotes");
            console.log(error);
            return false;
        }
    }
    public async navigateToSubPolicies() {
        try {
            await this.driverService.waitUntilElementLoaded(this.policiesBtnTab);
            await waitUntilHorizontalProgressBarLoaded_v2(this.driverService, 500);
            await this.driverService.click(this.policiesBtnTab);
            return true;
        } catch (error) {
            console.log("navigateToSubPolicies");
            console.log(error);
            return false;
        }
    }
    public async navigateToSubClaims() {
        try {
            await this.driverService.waitUntilElementLoaded(this.claimsBtnTab);
            await waitUntilHorizontalProgressBarLoaded_v2(this.driverService, 500);
            await this.driverService.click(this.claimsBtnTab);
            return true;
        } catch (error) {
            console.log("navigateToSubClaims");
            console.log(error);
            return false;
        }
    }
    public async navigateToSubDocuments() {
        try {
            await this.driverService.waitUntilElementLoaded(this.documentsBtnTab);
            await waitUntilHorizontalProgressBarLoaded_v2(this.driverService, 500);
            await this.driverService.click(this.documentsBtnTab);
            return true;
        } catch (error) {
            console.log("navigateToSubDocuments");
            console.log(error);
            return false;
        }
    }
    public async navigateToSubHistory() {
        try {
            await this.driverService.waitUntilElementLoaded(this.historyBtnTab);
            await waitUntilHorizontalProgressBarLoaded_v2(this.driverService, 500);
            await this.driverService.click(this.historyBtnTab);
            return true;
        } catch (error) {
            console.log("navigateToSubHistory");
            console.log(error);
            return false;
        }
    }
    public async navigateToSubCases() {
        try {
            await this.driverService.waitUntilElementLoaded(this.casesBtnTab);
            await waitUntilHorizontalProgressBarLoaded_v2(this.driverService, 500);
            await this.driverService.click(this.casesBtnTab);
            return true;
        } catch (error) {
            console.log("navigateToSubCases");
            console.log(error);
            return false;
        }
    }
    public async navigateToSubNotes() {
        try {
            await this.driverService.waitUntilElementLoaded(this.notesBtnTab);
            await waitUntilHorizontalProgressBarLoaded_v2(this.driverService, 500);
            await this.driverService.click(this.notesBtnTab);
            return true;
        } catch (error) {
            console.log("navigateToSubNotes");
            console.log(error);
            return false;
        }
    }
    public async navigateToSubSms() {
        try {
            await this.driverService.waitUntilElementLoaded(this.smsBtnTab);
            await waitUntilHorizontalProgressBarLoaded_v2(this.driverService, 500);
            await this.driverService.click(this.smsBtnTab);
            return true;
        } catch (error) {
            console.log("navigateToSubSms");
            console.log(error);
            return false;
        }
    }
    public async navigateToSubContacts() {
        try {
            await this.driverService.waitUntilElementLoaded(this.contactsBtnTab);
            await waitUntilHorizontalProgressBarLoaded_v2(this.driverService, 500);
            await this.driverService.click(this.contactsBtnTab);
            return true;
        } catch (error) {
            console.log("navigateToSubContacts");
            console.log(error);
            return false;
        }
    }
    public async navigateToSubEntities() {
        try {
            await this.driverService.waitUntilElementLoaded(this.entitiesBtnTab);
            await waitUntilHorizontalProgressBarLoaded_v2(this.driverService, 500);
            await this.driverService.click(this.entitiesBtnTab);
            return true;
        } catch (error) {
            console.log("navigateToSubEntities");
            console.log(error);
            return false;
        }
    }
    public async navigateToSubScoring() {
        try {
            await this.driverService.waitUntilElementLoaded(this.scoringBtnTab);
            await waitUntilHorizontalProgressBarLoaded_v2(this.driverService, 500);
            await this.driverService.click(this.scoringBtnTab);
            return true;
        } catch (error) {
            console.log("navigateToSubScoring");
            console.log(error);
            return false;
        }
    }
    //#endregion /*2. End -- Navigate to Sub tab*/

    //#region  /* 3. Begin -- On Form*/
    public async closeOpeningForm() {
        let btnCloseForm = By.xpath("//*[contains(local-name(), 'form')]//button[@aria-label='Close' or contains(@class,'close')]");
        let len = (await this.driverService.findElements(btnCloseForm)).length;
        for (let i = 1; i <= len; i++) {
            try {
                btnCloseForm = By.xpath(`(//*[contains(local-name(), 'form')]//button[@aria-label='Close' or contains(@class,'close')])[${i}]`);
                await this.driverService.click(btnCloseForm);
                await this.driverService.waitForSeconds(1000);
            } catch (error) {
                continue;
            }
        }
    }

    public async pressSaveForm() {
        try {
            await this.driverService.waitUntilElementLoaded(this.btnSaveForm);
            await waitUntilHorizontalProgressBarLoaded_v2(this.driverService, 500);
            await this.driverService.click(this.btnSaveForm);
            return true;
        } catch (error) {
            console.log("pressSaveForm");
            console.log(error);
            return false;
        }
    }
    public async pressCancelForm() {
        try {
            await this.driverService.waitUntilElementLoaded(this.btnCancelForm);
            await waitUntilHorizontalProgressBarLoaded_v2(this.driverService, 500);
            await this.driverService.click(this.btnCancelForm);
            return true;
        } catch (error) {
            console.log("pressCancelForm");
            console.log(error);
            return false;
        }
    }
    public async pressYesForm() {
        try {
            await this.driverService.waitUntilElementLoaded(this.btnYesForm);
            await waitUntilHorizontalProgressBarLoaded_v2(this.driverService, 500);
            await this.driverService.click(this.btnYesForm);
            await waitUntilHorizontalProgressBarLoaded(this.driverService);
            return true;
        } catch (error) {
            console.log("pressYesForm");
            console.log(error);
            return false;
        }
    }
    public async pressNoForm() {
        try {
            await this.driverService.waitUntilElementLoaded(this.btnNoForm);
            await waitUntilHorizontalProgressBarLoaded_v2(this.driverService, 500);
            await this.driverService.click(this.btnNoForm);
            return true;
        } catch (error) {
            console.log("pressNoForm");
            console.log(error);
            return false;
        }
    }
    public async pressRegisterForm() {
        try {
            await this.driverService.waitUntilElementLoaded(this.btnRegisterForm);
            await waitUntilHorizontalProgressBarLoaded_v2(this.driverService, 500);
            await this.driverService.click(this.btnRegisterForm);
            return true;
        } catch (error) {
            console.log("pressRegisterForm");
            console.log(error);
            return false;
        }
    }
    public async selectDropdownOption(optionName: string) {
        try {
            await waitUntilHorizontalProgressBarLoaded_v2(this.driverService, 500);
            await waitUntilHorizontalProgressBarLoaded_v2(this.driverService, 500);
            await selectDropdownOption(optionName, "", this.driverService);
            return true;
        } catch (error) {
            console.log("selectDropdownOption");
            console.log(error);
            return false;
        }
    }

    public async checkSaveButtonFormExist() {
        try {
            await waitUntilHorizontalProgressBarLoaded_v2(this.driverService, 100);
            return await this.driverService.isExisted(this.btnSaveForm);
        } catch (error) {
            console.log("checkSaveButtonFormExist");
            console.log(error);
            return false;
        }
    }

    public async checkCancelButtonFormExist() {
        try {
            await waitUntilHorizontalProgressBarLoaded_v2(this.driverService, 100);
            return await this.driverService.isExisted(this.btnCancelForm);
        } catch (error) {
            console.log("checkCancelButtonFormExist");
            console.log(error);
            return false;
        }
    }
    //#endregion /* 3. End -- On Form*/

    public async getCurrentUrl() {
        return await this.driverService.getCurrentUrl();
    }

    public async refreshPage() {
        await this.driverService.refreshPage();
    }

    public async backPage() {
        await this.driverService.back();
    }

    public async forwardPage() {
        await this.driverService.forward();
    }

    public async waitForProgressBarLoaded() {
        try {
            await waitUntilHorizontalProgressBarLoaded(this.driverService);
            return true;
        } catch (error) {
            console.log("waitForProgressBarLoaded");
            console.log(error);
            return false;
        }
    }
    public async waitForProgressBarLoaded_v2(millisecondNumber: number = 1000) {
        try {
            await waitUntilHorizontalProgressBarLoaded_v2(this.driverService, millisecondNumber);
            return true;
        } catch (error) {
            console.log("waitForProgressBarLoaded_v2");
            console.log(error);
            return false;
        }
    }
    public async checkProgressBarLoading(millisecondNumber: number = 5000) {
        try {
            await this.driverService.waitUntilElementLoaded(locator_progressbarActive, millisecondNumber);
            return true;
        } catch (error) {
            return false;
        }
    }
    public async waitForSeconds(millisecondNumber: number) {
        try {
            await this.driverService.waitForSeconds(millisecondNumber);
            return true;
        } catch (error) {
            console.log("waitForSeconds");
            console.log(error);
            return false;
        }
    }


    public async expandNumberOfItemMainList(numberItem: number = 30) {
        try {
            const btnItemPage = By.xpath("//div[contains(@class,'tab-pane') and contains(@class,'active')]//button[@id='pgs-expand-rows-btn']");
            await expandNumberItemOfList(this.driverService, btnItemPage, numberItem);
            return true;
        } catch (error) {
            console.log("expandNumberOfItemMainList");
            console.log(error);
            return false;
        }
    }

    public async checkColumnOnMainListExist(columnName: string) {
        const column = By.xpath(`//div[contains(@class,'tab-pane') and contains(@class,'active')]//table//th//span[text()='${columnName}']`);
        return await this.driverService.isExisted(column);
    }

    public async checkPaginationOnMainListExist() {
        const pagination = By.xpath(`//div[contains(@class,'tab-pane') and contains(@class,'active')]//c-table//ul[contains(@class,'pagination')]`);
        return await this.driverService.isExisted(pagination);
    }

    public async checkConfigColumnOnMainListExist() {
        const configColumn = By.xpath(`//div[contains(@class,'tab-pane') and contains(@class,'active')]//table//th[contains(@class,'config-column')]`);
        return await this.driverService.isExisted(configColumn);
    }
    public async reloadTable(timeWaitBeforeReload: number = 0) {
        try {
            await this.driverService.waitForSeconds(timeWaitBeforeReload);
            await waitUntilHorizontalProgressBarLoaded_v2(this.driverService, 500);
            await reloadTable(this.driverService);
            return true;
        } catch (error) {
            console.log("reloadItemMainList");
            console.log(error);
            return false;
        }
    }
    public async expandNumberOfItemSubList(numberItem: number = 30) {
        try {
            const btnItemPage = By.xpath("//div[contains(@class,'tab-pane') and contains(@class,'active')]//div[contains(@class,'tab-pane') and contains(@class,'active')]//button[@id='pgs-expand-rows-btn']");
            await expandNumberItemOfList(this.driverService, btnItemPage, numberItem);
            return true;
        } catch (error) {
            console.log("expandNumberOfItemSubList");
            console.log(error);
            return false;
        }
    }

    public async checkColumnOnSubListExist(columnName: string) {
        const column = By.xpath(`//div[contains(@class,'tab-pane') and contains(@class,'active')]//div[contains(@class,'tab-pane') and contains(@class,'active')]//table//th//span[text()='${columnName}']`);
        return await this.driverService.isExisted(column);
    }

    public async checkConfigColumnOnSubListExist() {
        const configColumn = By.xpath(`//div[contains(@class,'tab-pane') and contains(@class,'active')]//div[contains(@class,'tab-pane') and contains(@class,'active')]//table//th[contains(@class,'config-column')]`);
        return await this.driverService.isExisted(configColumn);
    }

    public async checkPaginationOnSubListExist() {
        const pagination = By.xpath(`//div[contains(@class,'tab-pane') and contains(@class,'active')]//c-table//ul[contains(@class,'pagination')]`);
        return await this.driverService.isExisted(pagination);
    }
    // Close all opening accounts, leads, sales, contacts, cases,...
    public async closeAllOpeningEntities() {
        try {
            const btnCloseEntity = By.xpath("(//*[@type='button' and @class='btn-close ng-star-inserted' or @class='btn-close'])[1]");
            while (await this.driverService.isExisted(btnCloseEntity)) {
                await this.driverService.waitUntilElementLoaded(btnCloseEntity);
                await this.driverService.click(btnCloseEntity);
                await this.driverService.waitForSeconds(500);
            }
            return true;
        } catch (error) {
            console.log("closeAllOpeningEntities\n" + error);
            return false;
        }
    }

    // Press key board
    public async pressEnterCurrentElement() {
        try {
            await this.driverService.pressEnterCurrentElement();
            return true;
        } catch (error) {
            console.log("pressEnterCurrentElement\n" + error);
            return false;
        }
    }

    public async pressTabCurrentElement() {
        try {
            await this.driverService.pressTabCurrentElement();
            return true;
        } catch (error) {
            console.log("pressTabCurrentElement\n" + error);
            return false;
        }
    }

    public async checkToastErrorExist() {
        return await this.driverService.isExisted(toastError);
    }

    public async checkToastSuccessExistWithMessage(message: string) {
        const toast = By.xpath(`//div[@id='toast-container']/div[contains(@class,'toast-success')]/div[contains(text(),'${message}')]`);
        return await this.driverService.isExisted(toast);
    }

    public async getTextToastSuccess() {
        return await this.driverService.getText(toastSuccess);
    }
    //Input: column name
    //Output: Index of that column in table. Start index = 1
    public async getIndexColumnByName(columnName: string, rootXpath: string = "") {
        let result = -1;
        const header = By.xpath(rootXpath + "//table//th//span[(text())]");
        const len = await (await this.driverService.findElements(header)).length;
        for (let i = 1; i <= len; i++) {
            const actualName = await this.driverService.getText(By.xpath(`(${rootXpath}//table//th//span[(text())])[${i}]`));
            if (columnName.localeCompare(actualName) === 0) {
                result = i;
                break;
            }
        }
        return result;
    }


    //#region /* Begin: Search & Filter form*/
    public async pressSearchSearchAndFilter() {
        try {
            await this.driverService.waitUntilElementLoaded(this.btnSearchSearchAndFilter);
            await closeToastMessage(this.driverService);
            await waitUntilHorizontalProgressBarLoaded_v2(this.driverService, 500);
            await this.driverService.click(this.btnSearchSearchAndFilter);
            return true;
        } catch (error) {
            console.log("pressSearchSearchAndFilter");
            console.log(error);
            return false;
        }
    }

    public async pressSaveSearchAndFilter() {
        try {
            await this.driverService.waitUntilElementLoaded(this.btnSaveSearchAndFilter);
            await closeToastMessage(this.driverService);
            await waitUntilHorizontalProgressBarLoaded_v2(this.driverService, 500);
            await this.driverService.click(this.btnSaveSearchAndFilter);
            return true;
        } catch (error) {
            console.log("pressSaveSearchAndFilter");
            console.log(error);
            return false;
        }
    }

    public async pressClearSearchAndFilter() {
        try {
            await this.driverService.waitUntilElementLoaded(this.btnClearSearchAndFilter);
            await closeToastMessage(this.driverService);
            await waitUntilHorizontalProgressBarLoaded_v2(this.driverService, 500);
            await this.driverService.click(this.btnClearSearchAndFilter);
            return true;
        } catch (error) {
            console.log("pressClearSearchAndFilter");
            console.log(error);
            return false;
        }
    }

    public async pressCancelSearchAndFilter() {
        try {
            await this.driverService.waitUntilElementLoaded(this.btnCancelSearchAndFilter);
            await closeToastMessage(this.driverService);
            await waitUntilHorizontalProgressBarLoaded_v2(this.driverService, 500);
            await this.driverService.click(this.btnCancelSearchAndFilter);
            return true;
        } catch (error) {
            console.log("pressCancelSearchAndFilter");
            console.log(error);
            return false;
        }
    }

    //#endregion /* End: Search & Filter form*/

    //#region /*Begin: Filter Name form*/
    public async inputFilterNameFilterNameForm(FilterName: string) {
        try {
            await this.driverService.waitUntilElementLoaded(this.txtFilterNameFilterNameForm);
            await waitUntilHorizontalProgressBarLoaded_v2(this.driverService, 100);
            await this.driverService.setText(this.txtFilterNameFilterNameForm, FilterName);
            return true;
        } catch (error) {
            console.log("inputFilterNameFilterNameForm");
            console.log(error);
            return false;
        }
    }

    public async selectFilterTypeFilterNameForm(FilterType: string) {
        try {
            await this.driverService.waitUntilElementLoaded(this.rdbGlobalFilterFilterNameForm);
            await waitUntilHorizontalProgressBarLoaded_v2(this.driverService, 100);
            if (FilterType.localeCompare("Global Filter") === 0 || FilterType.toLowerCase().includes("global")) {
                await this.driverService.click(this.rdbGlobalFilterFilterNameForm);
            }
            else {
                await this.driverService.click(this.rdbPrivateFilterFilterNameForm);
            }
            return true;
        } catch (error) {
            console.log("selectFilterTypeFilterNameForm");
            console.log(error);
            return false;
        }
    }
    //#endregion /*End: Filter Name form*/

    //#region /*Begin: Active tab*/
    public async pressSaveTab() {
        try {
            await this.driverService.waitUntilElementLoaded(this.btnSaveTab);
            await waitUntilHorizontalProgressBarLoaded_v2(this.driverService, 500);
            await this.driverService.click(this.btnSaveTab);
            return true;
        } catch (error) {
            console.log("pressSaveTab");
            console.log(error);
            return false;
        }
    }

    // Sub tab
    public async getNumberOfTotalRecordsSubTab() {
        try {
            await this.driverService.waitUntilElementLoaded(this.lblTotalNumberRecordSubTab);
            await waitUntilHorizontalProgressBarLoaded_v2(this.driverService, 500);
            let temp = await this.driverService.getText(this.lblTotalNumberRecordSubTab);
            let result = parseInt(temp.replace(/^\D+/g, ''));
            return result;
        } catch (error) {
            console.log("getNumberOfTotalRecordsSubTab");
            console.log(error);
            return -1;
        }
    }

    // Main tab
    public async getNumberOfTotalRecordsMainTab() {
        try {
            await this.driverService.waitUntilElementLoaded(this.lblTotalNumberRecordMainTab);
            await waitUntilHorizontalProgressBarLoaded_v2(this.driverService, 500);
            let temp = await this.driverService.getText(this.lblTotalNumberRecordMainTab);
            let result = parseInt(temp.replace(/^\D+/g, ''));
            return result;
        } catch (error) {
            console.log("getNumberOfTotalRecordsMainTab");
            console.log(error);
            return -1;
        }
    }
    //#endregion /*End: Active tab*/

    //#region /*Begin: Check button menu header exist*/
    public async checkDashboardButtonMenuExist() {
        try {
            await waitUntilHorizontalProgressBarLoaded_v2(this.driverService, 500);
            return await this.driverService.isExisted(this.dashboardBtnMenu);
        } catch (error) {
            console.log("checkDashboardButtonMenuExist");
            console.log(error);
            return false;
        }
    }

    public async checkClaimButtonMenuExist() {
        try {
            await waitUntilHorizontalProgressBarLoaded_v2(this.driverService, 500);
            return await this.driverService.isExisted(this.claimsBtnMenu);
        } catch (error) {
            console.log("checkClaimButtonMenuExist");
            console.log(error);
            return false;
        }
    }
    /*End: Check button menu header exist*/
    //#endregion

    //#region /*Methods at Module configuration - Global setting*/
    /**
     * we will open an item at Global setting
     * @param nameOfDomainItem 
     */
    public async selectDomainCardAtGlobalSetting(nameOfDomainItem: string) {
        try {
            const domainItem = By.xpath(`//div[contains(@class,'tab-pane') and contains(@class,'active')]//app-domain-card//div[text()='${nameOfDomainItem}']`);
            await this.driverService.waitUntilElementLoaded(domainItem);
            await waitUntilHorizontalProgressBarLoaded_v2(this.driverService, 500);
            await this.driverService.click(domainItem);
            return true;
        } catch (error) {
            console.log('selectDomainCardAtGlobalSetting');
            console.log(error);
            return false;
        }
    }

    /**
     * We will press Setting button at a Domain detail item in a Domain card at Global Setting
     * @param nameOfDomainDetailItem 
     */
    public async pressSettingAtDomainDetailItemInDomainCard(nameOfDomainDetailItem: string) {
        try {
            const domainDetailItem = By.xpath(`//div[contains(@class,'tab-pane') and contains(@class,'active')]//app-configuration-list//div[contains(@class,'domain-detail-item') and .//h4[text()='${nameOfDomainDetailItem}']]//button[contains(text(),'Settings')]`);
            await this.driverService.waitUntilElementLoaded(domainDetailItem);
            await waitUntilHorizontalProgressBarLoaded_v2(this.driverService, 500);
            await this.driverService.click(domainDetailItem);
            return true;
        } catch (error) {
            console.log('pressSettingAtDomainDetailItemInDomainCard');
            console.log(error);
            return false;
        }
    }
    //#endregion
}
