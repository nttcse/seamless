import { By, Key } from "selenium-webdriver";
import { SeleniumWebDriverService } from "../core/selenium-webdriver.service";
import { fail, strictEqual, notStrictEqual } from "assert";
import { log } from "console";
import { scenarioName, toastError } from '../shared/variables';
import { GlobalPageObject } from "./global-page-object";
import {
    reloadTable,
    waitUntilHorizontalProgressBarLoaded, 
    waitUntilHorizontalProgressBarLoaded_v2, 
    logWarningMessage, 
    logSuccessMessage, 
    closeToastMessage, 
    selectDropdownOption, 
    logInfoMessage 
} from '../shared/functions';

export class AccountPage {
    private globalPageObject = new GlobalPageObject(this.driverService);

    private btnCreateNewAccount = By.xpath("//*[@id='create-customer-dropdown']");
    private btnPerson = By.xpath("//button[contains(.,'Person')]");
    private btnCompany = By.xpath("//button[contains(.,'Company')]");

    //Locator of elements on "create account person" form
    private txtNIN = By.xpath("//input[contains(@class,'pgs-json-schema-control-nin')]");
    private txtFirstName = By.xpath("//input[contains(@class,'pgs-json-schema-control-first-name')]");
    private txtLastName = By.xpath("//input[contains(@class,'pgs-json-schema-control-last-name')]");
    private dtpDOB = By.xpath("//input[contains(@class,'pgs-json-schema-control-dob')]");
    private cmbGender = By.xpath("//ng-select[contains(@class,'pgs-json-schema-control-gender')]//input");
    private txtAddress = By.xpath("//input[contains(@class,'pgs-json-schema-control-address')]");
    private txtPostcode = By.xpath("//input[contains(@class,'pgs-json-schema-control-postcode')]"); //*[@id="pgs-json-schema-control-Postcode"]
    private txtCity = By.xpath("//input[contains(@class,'pgs-json-schema-control-city')]");
    private cmbCountry = By.xpath("//ng-select[contains(@class,'pgs-json-schema-control-country')]//input");
    private txtEmailAddress = By.xpath("//input[contains(@class,'pgs-json-schema-control-email-address')]");
    private txtPhoneNumber = By.xpath("//input[contains(@class,'pgs-json-schema-control-phone-number')]");
    private cmbPreferredCommunication = By.xpath("//ng-select[contains(@class,'pgs-json-schema-control-preferred-communication')]//input");
    private cmbStatus = By.xpath("//ng-select[contains(@class,'pgs-json-schema-control-status')]//input");
    private cmbKAM = By.xpath("//ng-select[contains(@class,'pgs-json-schema-control-kam')]//input");
    private cmbPaymentType = By.xpath("//ng-select[contains(@class,'pgs-json-schema-control-payment-type')]//input");

    //PaymentFrequency is changed to PaymentFrequency
    private cmbPaymentFrequency = By.xpath("//ng-select[contains(@class,'pgs-json-schema-control-payment-frequency')]//input");
    private cmbPreferredCollectionDate = By.xpath("//ng-select[contains(@class,'pgs-json-schema-control-preferred-collection-date')]//input");

    //Locator of elements on "create account company" form
    private txtOrgNo = By.xpath("//input[contains(@class,'pgs-json-schema-control-org.-no')]");
    private btnSearchOrgNo = By.xpath("//input[contains(@class,'pgs-json-schema-control-org.-no')]/following-sibling::span");
    private txtCompanyName = By.xpath("//input[contains(@class,'pgs-json-schema-control-company-name')]");
    private txtEmailAddressCompany = By.xpath("//input[contains(@class,'pgs-json-schema-control-email-address')]");
    private txtCompanyPhone = By.xpath("//input[contains(@class,'pgs-json-schema-control-company-phone')]");
    private cmbStatusCompany = By.xpath("//ng-select[contains(@class,'pgs-json-schema-control-status')]//input");
    private cmbCountryCompany = By.xpath("//ng-select[contains(@class,'pgs-json-schema-control-country')]//input");

    private txtVisitingAddress = By.xpath("//input[contains(@class,'pgs-json-schema-control-visiting-address')]");
    private txtVisitingExtraAddress = By.xpath("(//input[contains(@class,'pgs-json-schema-control-extra-address')])[1]");
    private txtVisitingPostcode = By.xpath("(//input[contains(@class,'pgs-json-schema-control-postcode')])[1]");
    private txtVisitingCity = By.xpath("(//input[contains(@class,'pgs-json-schema-control-city')])[1]");
    private txtPostalAddress = By.xpath("//input[contains(@class,'pgs-json-schema-control-postal-address')]");
    private txtPostalExtraAddress = By.xpath("(//input[contains(@class,'pgs-json-schema-control-extra-address')])[2]");
    private txtPostalPostcode = By.xpath("(//input[contains(@class,'pgs-json-schema-control-postcode')])[2]");
    private txtPostalCity = By.xpath("(//input[contains(@class,'pgs-json-schema-control-city')])[2]");
    private txtInvoiceAddress = By.xpath("//input[contains(@class,'pgs-json-schema-control-invoice-address')]");
    private txtInvoiceExtraAddress = By.xpath("(//input[contains(@class,'pgs-json-schema-control-extra-address')])[3]");
    private txtInvoicePostcode = By.xpath("(//input[contains(@class,'pgs-json-schema-control-postcode')])[3]");
    private txtInvoiceCity = By.xpath("(//input[contains(@class,'pgs-json-schema-control-city')])[3]");

    private cmbCreditRating = By.xpath("//input[contains(@class,'pgs-json-schema-control-credit-score')]");
    private cmbIndustry = By.xpath("(//ng-select[contains(@class,'pgs-json-schema-control-industry')]//input)[2]");
    private cmbScoringCompany = By.xpath("//*[@id='json-schema-control-Scoring']//input");
    private cmbSourceCompany = By.xpath("//*[@id='json-schema-control-Source']//input");
    private cmbKAMCompany = By.xpath("//ng-select[contains(@class,'pgs-json-schema-control-kam')]//input");
    private cmbPaymentTypeCompany = By.xpath("//ng-select[contains(@class,'pgs-json-schema-control-payment-type')]//input");
    private cmbPaymentFrequencyCompany = By.xpath("//ng-select[contains(@class,'pgs-json-schema-control-payment-frequency')]//input");
    private cmbPreferredCollectionDateCompany = By.xpath("//ng-select[contains(@class,'pgs-json-schema-control-preferred-collection-date')]//input");

    private txtCreditScore = By.xpath("//input[contains(@class,'pgs-json-schema-control-credit-score')]");

    private cmbIndustryCode = By.xpath("//ng-select[contains(@class,'pgs-json-schema-control-industry-code')]//input");

    private cmbEducationaLevel = By.xpath("//ng-select[contains(@class,'pgs-json-schema-control-educational-level')]//input");

    private dtpCompanyRegistrationDate = By.xpath("//input[contains(@class,'pgs-json-schema-control-company-registration-date')]");

    private btnCloseEditAccountForm = By.xpath("//ngb-modal-window//button[@aria-label='Close' and @class='close']");

    //Locator for elements at first row of account list
    private lblNameAccount = By.xpath("(//table//tr[1]//td[contains(@class,'pgs-cus-name')]//*[self::*[text()]])[last()]");

    //Search
    private btnSearchAndFilter = By.xpath("//button//span[text()='Search & Filter']");
    private btnClearSearchFilter = By.xpath("//app-customer-filter//button[text()='Clear ']");

    //Element at Search and Filter
    //private btnSearchAndFilter = By.xpath("//button//span[text()='Search & Filter']");
    private txtNameSearchFilter = By.xpath("//input[@id='pgs-account-filter-fullname']");
    private txtOrgNo_NINSearchFilter = By.xpath("//input[@id='pgs-account-filter-ssn']");
    private txtEmailSearchFilter = By.xpath("//input[@id='pgs-account-filter-email']");
    private txtPhoneSearchFilter = By.xpath("//input[@id='pgs-account-filter-phone']");
    private cmbKAMSearchFilter = By.xpath("//input[@id='pgs-account-filter-kam']");
    private cmbStatusSearchFilter = By.xpath("//input[@id='pgs-account-filter-status']");
    private cmbTypeSearchFilter = By.xpath("//input[@id='pgs-account-filter-type']");
    private txtAddressSearchFilter = By.xpath("//input[@id='pgs-account-filter-address']");
    private txtPostcodeSearchFilter = By.xpath("//input[@id='pgs-account-filter-postcode']");
    private txtCitySearchFilter = By.xpath("//input[@id='pgs-account-filter-city']");
    private cmbCountrySearchFilter = By.xpath("//input[@id='pgs-account-filter-country']");

    private txtQuoteReferenceSearchFilter = By.xpath("//input[@id='pgs-account-filter-quote-number']");
    private txtPolicyReferenceSearchFilter = By.xpath("//input[@id='pgs-account-filter-policy-number']");
    private dtpPolicyStartDateFromSearchFilter = By.xpath("//app-customer-filter//formly-wrapper-form-field[.//*[contains(text(),'Policy start date from')]]//input");
    private dtpPolicyStartDateToSearchFilter = By.xpath("//app-customer-filter//formly-wrapper-form-field[.//*[contains(text(),'Policy start date to')]]//input");
    private dtpPolicyEndDateFromSearchFilter = By.xpath("//app-customer-filter//formly-wrapper-form-field[.//*[contains(text(),'Policy end date from')]]//input");
    private dtpPolicyEndDateToSearchFilter = By.xpath("//app-customer-filter//formly-wrapper-form-field[.//*[contains(text(),'Policy end date to')]]//input");
    private cmbProductSearchFilter = By.xpath("//app-customer-filter//formly-wrapper-form-field[.//*[contains(text(),'Product')]]//input");

    private btnSearchSearchFilter = By.xpath("//app-customer-filter//span[contains(@class,'fa-loading')]");
    //private btnClearSearchFilter = By.xpath("//app-customer-filter//button[text()='Clear ']");
    private btnSaveSearchFilter = By.xpath("//app-customer-filter//button[not (@disabled)]/span[text()='Save']");

    private btnCloseSearchAndFilterForm = By.xpath("//app-customer-filter//div[contains(@class,'card-header')]/button/i[contains(@class,'fa-times')]");

    private accountbtn = By.xpath("(//a[@class='text-truncate'])[1]");

    constructor(private driverService: SeleniumWebDriverService) { }

    public async openCreateNewAccountForm() {
        try {
            await this.driverService.waitUntilElementLoaded(this.btnCreateNewAccount);
            await this.driverService.waitForSeconds(1000);
            await this.driverService.click(this.btnCreateNewAccount);
            //await this.driverService.waitForSeconds(500);
            // await this.driverService.waitUntilElementLoaded(this.btnCompany);
            // await this.driverService.click(this.btnCompany);
            return true;
        } catch (error) {
            console.log("Open Create New Account Company form");
            console.log(error);
            return false;
        }
    }

    public async inputDataToCreateAccountForm(
        OrgNo: string,
        CompanyName: string,
        EmailAddress: string,
        CompanyPhone: string,
        Status: string,
        Country: string,
        VisitingAddress: string,
        VisitingExtraAddress: string,
        VisitingPostcode: string,
        VisitingCity: string,
        PostalAddress: string,
        PostalExtraAddress: string,
        PostalPostcode: string,
        PostalCity: string,
        InvoiceAddress: string,
        InvoiceExtraAddress: string,
        InvoicePostcode: string,
        InvoiceCity: string,
        CreditScore: string,
        CreditRating: string,
        IndustryCode: string,
        Industry: string,
        EducationalLevel: string,
        CompanyRegistrationDate: string,
        KAM: string,
        PreferredCollectionDate: string,
        PaymentType: string,
        PaymentFrequency: string
    ) {
        try {
            await this.driverService.waitUntilElementLoaded(this.txtOrgNo);

            //input new data from file
            await this.driverService.setText(this.txtOrgNo, OrgNo);
            await this.driverService.setText(this.txtCompanyName, CompanyName);

            await this.driverService.setText(this.txtEmailAddressCompany, EmailAddress);
            await this.driverService.setText(this.txtCompanyPhone, CompanyPhone);
            await this.driverService.setText(this.cmbStatusCompany, Status);
            await this.driverService.pressEnterCurrentElement();

            await this.driverService.setText(this.txtVisitingAddress, VisitingAddress);
            await this.driverService.setText(this.txtVisitingExtraAddress, VisitingExtraAddress);
            await this.driverService.setText(this.txtVisitingPostcode, VisitingPostcode);
            await this.driverService.setText(this.txtVisitingCity, VisitingCity);

            await this.driverService.setText(this.txtPostalAddress, PostalAddress);
            await this.driverService.setText(this.txtPostalExtraAddress, PostalExtraAddress);
            await this.driverService.setText(this.txtPostalPostcode, PostalPostcode);
            await this.driverService.setText(this.txtPostalCity, PostalCity);

            await this.driverService.setText(this.txtInvoiceAddress, InvoiceAddress);
            await this.driverService.setText(this.txtInvoiceExtraAddress, InvoiceExtraAddress);
            await this.driverService.setText(this.txtInvoicePostcode, InvoicePostcode);
            await this.driverService.setText(this.txtInvoiceCity, InvoiceCity);

            await this.driverService.setText(this.txtCreditScore, CreditScore);

            await this.driverService.setText(this.cmbCreditRating, CreditRating);
            await this.driverService.pressEnterCurrentElement();

            await this.driverService.setText(this.cmbIndustryCode, IndustryCode);
            await this.driverService.pressEnterCurrentElement();

            await this.driverService.setText(this.cmbIndustry, Industry);
            await this.driverService.pressEnterCurrentElement();

            await this.driverService.setText(this.cmbEducationaLevel, EducationalLevel);
            await this.driverService.pressEnterCurrentElement();

            await this.driverService.setText(this.dtpCompanyRegistrationDate, CompanyRegistrationDate);
            await this.driverService.pressTabCurrentElement();

            //KAM
            await this.driverService.click(this.cmbKAM);
            await waitUntilHorizontalProgressBarLoaded(this.driverService);
            await this.driverService.setText(this.cmbKAM, KAM);
            await waitUntilHorizontalProgressBarLoaded(this.driverService);
            await selectDropdownOption(KAM, "", this.driverService);
            await this.driverService.pressEnter(this.cmbKAM);

            await this.driverService.setText(this.cmbPreferredCollectionDateCompany, PreferredCollectionDate);
            await this.driverService.pressEnterCurrentElement();

            await this.driverService.setText(this.cmbPaymentTypeCompany, PaymentType);
            await this.driverService.pressEnterCurrentElement();
            await this.driverService.setText(this.cmbPaymentFrequencyCompany, PaymentFrequency);
            await this.driverService.pressEnterCurrentElement();
            return true;
        } catch (error) {
            console.log("Input data to Create Account form");
            logWarningMessage("Input data to Create Account form: failed!");
            console.log(error);
            return false;
        }
    }

    public async closeEditAccountForm() {
        if ((await this.driverService.isExisted(this.btnCloseEditAccountForm))) {
            await this.driverService.click(this.btnCloseEditAccountForm);
            await this.driverService.waitForSeconds(1000);
        }
    }

    public async closeCreateAccountCompanyForm() {
        if ((await this.driverService.isExisted(this.txtFirstName))) {
            await this.driverService.waitUntilElementLoaded(this.txtFirstName);
            await this.driverService.click(By.xpath("//button[@class='close']"));
            await this.driverService.waitForSeconds(1000);
        }
    }

    public async reloadAccountList() {
        try {
            await this.driverService.waitForSeconds(10000);
            await reloadTable(this.driverService);
            await this.driverService.waitForSeconds(2000);
            await reloadTable(this.driverService);
        } catch (error) {
            console.log(error);
        }
    }

    public async assertCreateAccount(
        positionRow: number,
        name: string,
        email: string,
        phone: string,
        address: string,
        KAM: string,
        status: string
    ) {
        let actualName: string = "";
        let actualEmail: string = "";
        let actualPhone: string = "";
        let actualAddress: string = "";
        let actualKAM: string = "";
        let actualStatus: string = "";
        try {
            //Locator for elements at row {positionRow} of account list
            let lblNameAccount = By.xpath(`(//table//tr[${positionRow}]//td[contains(@class,'pgs-cus-name')]//*[self::*[text()]])[last()]`);
            let lblEmailAccount = By.xpath(`//table//tr[${positionRow}]//td[contains(@class,'pgs-cus-email')]//*[self::*[text()]]`);
            let lblPhoneAccount = By.xpath(`//table//tr[${positionRow}]//td[contains(@class,'pgs-cus-phone')]//*[self::*[text()]]`);
            let lblAddressAccount = By.xpath(`//table//tr[${positionRow}]//td[contains(@class,'pgs-cus-address')]//*[self::*[text()]]`);
            let lblKAM = By.xpath(`//table//tr[${positionRow}]//td[contains(@class,'pgs-cus-kam')]//*[self::*[text()]]`);
            let lblStatusAccount = By.xpath(`//table//tr[${positionRow}]//td[contains(@class,'pgs-cus-status')]//*[self::*[text()]]`);

            await this.driverService.waitUntilElementLoaded(lblNameAccount);
            await this.driverService.waitForSeconds(10000);
            actualName = await (
                await this.driverService.findElement(lblNameAccount)
            ).getText();
            actualEmail = await (
                await this.driverService.findElement(lblEmailAccount)
            ).getText();
            actualPhone = await (
                await this.driverService.findElement(lblPhoneAccount)
            ).getText();
            actualAddress = await (
                await this.driverService.findElement(lblAddressAccount)
            ).getText();
            actualKAM = await this.driverService.getText(lblKAM);
            actualStatus = await (
                await this.driverService.findElement(lblStatusAccount)
            ).getText();
        } catch (error) {
            console.log("Assert Create account");
            console.log(error);
        }

        await this.driverService.validateTestCase(
            "Create account successfully!",
            [actualName, name, "Assert at Name: Incorrect Name!"],
            [actualEmail, email, "Assert at Email: Incorrect Email!"],
            [actualPhone, phone, "Assert at Phone: Incorrect Phone!"],
            [actualAddress, address, "Assert at Address: Incorrect Address!"],
            [actualKAM, KAM, "Assert at KAM: Incorrect KAM!"],
            [actualStatus, status, "Assert at Status: Incorrect Status!"]
        );
    }

    public async openEditFormOfFirstAccountPage1(): Promise<string> {
        try {
            logInfoMessage('\tOpening form edit of first Account...\n');
            await this.driverService.waitUntilElementLoaded(By.xpath('(//app-account-name-column//a)[1]'));
            let btnEdit = By.xpath(`(//app-customer-act-column/button)[1]`);
            await this.driverService.click(btnEdit);
            await waitUntilHorizontalProgressBarLoaded(this.driverService);
            await waitUntilHorizontalProgressBarLoaded_v2(this.driverService);
            await waitUntilHorizontalProgressBarLoaded_v2(this.driverService);
            if ((await this.driverService.isExisted(this.txtCompanyName))) {
                return 'company'
            }
            else {
                return 'person'
            }
        } catch (error) {
            console.log("openEditAccountFormByName");
            console.log(error);
            return 'undefined'
        }
    }

    public async openEditFormOfLastAccountPage1(): Promise<string> {
        try {
            logInfoMessage('\tOpening form edit of last Account...\n');
            await this.driverService.waitUntilElementLoaded(By.xpath('(//app-account-name-column//a)[1]'));
            for (let i = 30; i >= 1; i--) {
                if ((await this.driverService.isExisted(By.xpath(`(//app-account-name-column//a)[${i}]`)))) {
                    let btnEdit = By.xpath(`(//app-customer-act-column/button//*[contains(@class,'fa-edit')])[${i}]`);
                    await this.driverService.click(btnEdit);
                    await waitUntilHorizontalProgressBarLoaded(this.driverService);
                    if ((await this.driverService.isExisted(this.txtCompanyName))) {
                        return 'company'
                    }
                    else {
                        return 'person'
                    }
                }
            }
            logWarningMessage(`Can't open the last account!`);
            return 'undefined'
        } catch (error) {
            console.log("openEditAccountFormByName");
            console.log(error);
            return 'undefined'
        }
    }

    public async openDetailOfFirstAccount() {
        try {
            await this.driverService.waitUntilElementLoaded(this.lblNameAccount);
            await this.driverService.click(this.lblNameAccount);
            await waitUntilHorizontalProgressBarLoaded_v2(this.driverService);
            return true;
        } catch (error) {
            console.log("openDetailOfFirstAccount");
            console.log(error);
            return false;
        }
    }

    //Search and Filter
    public async openSearchAndFilterForm() {
        try {
            let formSearchAndFilterExpanded = By.xpath("//div[contains(@class,'show-right-side')]");
            if (!(await this.driverService.isExisted(formSearchAndFilterExpanded))) {
                await this.driverService.waitUntilElementLoaded(this.btnSearchAndFilter);
                await this.driverService.click(this.btnSearchAndFilter);
                await this.driverService.waitUntilElementLoaded(formSearchAndFilterExpanded);
                await waitUntilHorizontalProgressBarLoaded_v2(this.driverService);
            }
            return true;
        } catch (error) {
            console.log("openSearchAndFilterForm");
            console.log(error);
            return false;
        }
    }

    public async pressClearAtSearchAndFilter() {
        try {
            await this.driverService.waitUntilElementLoaded(this.btnClearSearchFilter);
            await closeToastMessage(this.driverService);
            await this.driverService.click(this.btnClearSearchFilter);
            await waitUntilHorizontalProgressBarLoaded(this.driverService);
            if (await this.driverService.isExisted(By.xpath("//app-customer-filter//formly-validation-message"))) {
                logWarningMessage("Validation message has NOT been deleted after press Clear filter");
                return false;
            }
            return true;
        } catch (error) {
            console.log("pressClearAtSearchAndFilter");
            console.log(error);
            return false;
        }
    }

    public async inputValidDataToSearchAndFilterForm(
        Name: string,
        OrgNo_NIN: string,
        Email: string,
        Phone: string,
        KAM: string,
        Status: string,
        Type: string,
        Address: string,
        Postcode: string,
        City: string,
        Country: string,
        QuoteReference: string,
        PolicyReference: string,
        PolicyStartDateFrom: string,
        PolicyStartDateTo: string,
        PolicyEndDateFrom: string,
        PolicyEndDateTo: string,
        Product: string
    ): Promise<boolean> {
        try {
            let lblNoItemsFound = By.xpath("//app-customer-filter//div[text()='No items found']");
            await this.driverService.waitUntilElementLoaded(this.txtNameSearchFilter);
            await waitUntilHorizontalProgressBarLoaded_v2(this.driverService);

            await this.fillDataIfHasValue(this.txtNameSearchFilter, Name);
            
            if (!(await this.validateTextInputFor(this.txtOrgNo_NINSearchFilter, OrgNo_NIN, 'pgs-account-filter-ssn'))) {
                logWarningMessage(`${OrgNo_NIN}: Format of Org.No/NIN is not correct!`);
                return false;
            }

            if (!(await this.validateTextInputFor(this.txtEmailSearchFilter, Email, 'pgs-account-filter-email'))) {
                logWarningMessage(`${Email}: Format of Email is not correct!`);
                return false;
            }

            if (!(await this.validateTextInputFor(this.txtPhoneSearchFilter, Phone, 'pgs-account-filter-phone'))) {
                logWarningMessage(`${Phone}: Format of Phone is not correct!`);
                return false;
            }

            if (KAM) {
                await this.driverService.click(this.cmbKAMSearchFilter);
                if (!(await this.driverService.isExisted(By.xpath("//*[@id='pgs-account-filter-kam' and (@aria-activedescendant)]")))) {
                    await waitUntilHorizontalProgressBarLoaded(this.driverService);
                }
                await this.driverService.setText(this.cmbKAMSearchFilter, KAM);
                await waitUntilHorizontalProgressBarLoaded(this.driverService);
                if (await this.driverService.isExisted(lblNoItemsFound)) {
                    logWarningMessage(`Not found KAM with name "${KAM}"`);
                    return false;
                }
                await selectDropdownOption(KAM, "", this.driverService);
            }

            if (!(await this.selectDropdownOptionWaitUntilHorizontalProgressBarLoaded_v2(this.cmbStatusSearchFilter, Status, lblNoItemsFound))) {
                logWarningMessage(`Not found Status with name: '${Status}'`);
                return false;
            }

            if (!(await this.selectDropdownOptionWaitUntilHorizontalProgressBarLoaded_v2(this.cmbTypeSearchFilter, Type, lblNoItemsFound))) {
                logWarningMessage(`Not found Type with name: '${Type}'`);
                return false;
            }

            await this.fillDataIfHasValue(this.txtAddressSearchFilter, Address);
            await this.fillDataIfHasValue(this.txtPostcodeSearchFilter, Postcode);
            await this.fillDataIfHasValue(this.txtCitySearchFilter, City);

            if (!(await this.selectDropdownOptionWaitUntilHorizontalProgressBarLoaded_v2(this.cmbCountrySearchFilter, Country, lblNoItemsFound))) {
                logWarningMessage(`Not found Country with name: '${Country}'`);
                return false;
            }

            await this.fillDataIfHasValue(this.txtQuoteReferenceSearchFilter, QuoteReference);
            await this.fillDataIfHasValue(this.txtPolicyReferenceSearchFilter, PolicyReference);
            await this.fillDataIfHasValue(this.dtpPolicyStartDateFromSearchFilter, PolicyStartDateFrom);
            await this.fillDataIfHasValue(this.dtpPolicyStartDateToSearchFilter, PolicyStartDateTo);
            await this.fillDataIfHasValue(this.dtpPolicyEndDateFromSearchFilter, PolicyEndDateFrom);
            await this.fillDataIfHasValue(this.dtpPolicyEndDateToSearchFilter, PolicyEndDateTo);

            if (!(await this.selectDropdownOptionWaitUntilHorizontalProgressBarLoaded_v2(this.cmbProductSearchFilter, Product, lblNoItemsFound))) {
                logWarningMessage(`Not found Product with name: '${Product}'`);
                return false;
            }

            return true;
        } catch (error) {
            console.log("inputDataToSearchAndFilterForm");
            console.log(error);
            return false;
        }
    }

    public async pressSearchAtSearchAndFilter(): Promise<boolean> {
        try {
            await this.driverService.waitUntilElementLoaded(this.btnSearchSearchFilter);
            await closeToastMessage(this.driverService);
            await this.driverService.click(this.btnSearchSearchFilter);
            if (await this.driverService.isExisted(By.xpath("//app-customer-filter//formly-validation-message"))) {
                logWarningMessage("Input data is not correct format!");
                return false;
            }
            return true;
        } catch (error) {
            console.log(error);
            return false;
        }
    }

    public async inputInvalidDataToSearchAndFilterForm(
        OrgNo_NIN: string,
        Email: string,
        Phone: string,
        PolicyStartDateFrom: string,
        PolicyStartDateTo: string,
        PolicyEndDateFrom: string,
        PolicyEndDateTo: string,
    ): Promise<boolean> {
        try {
            await this.driverService.waitUntilElementLoaded(this.txtNameSearchFilter);
            await this.driverService.waitUntilElementLoaded(By.xpath("//ng-progress/div[@class='ng-progress-bar']"));

            if (!(await this.validateInvalidTextInputFor(this.txtOrgNo_NINSearchFilter, OrgNo_NIN, 'pgs-account-filter-ssn'))) {
                logWarningMessage(`OrgNo/NIN: Should be shows validation error message when input incorrect format of OrgNo/NIN!. The input value: ${OrgNo_NIN}`);
                return false;
            }

            if (!(await this.validateInvalidTextInputFor(this.txtEmailSearchFilter, Email, 'pgs-account-filter-email'))) {
                logWarningMessage(`Email: Should be shows validation error message when input incorrect format of Email!. The input value: ${Email}`);
                return false;
            }

            if (!(await this.validateInvalidTextInputFor(this.txtPhoneSearchFilter, Phone, 'pgs-account-filter-phone'))) {
                logWarningMessage(`Phone: Should be shows validation error message when input incorrect format of Phone!. The input value: ${Phone}`);
                return false;
            }

            if (!(await this.validateInvalidTextInputContains(this.dtpPolicyStartDateFromSearchFilter, PolicyStartDateFrom, 'Policy start date from'))) {
                logWarningMessage(`PolicyStartDateFrom: Should be shows validation error message when input incorrect format!. The input value: ${PolicyStartDateFrom}`);
                return false;
            }

            if (!(await this.validateInvalidTextInputContains(this.dtpPolicyStartDateToSearchFilter, PolicyStartDateTo, 'Policy start date to'))) {
                logWarningMessage(`PolicyStartDateTo: Should be shows validation error message when input incorrect format!. The input value: ${PolicyStartDateTo}`);
                return false;
            }

            if (!(await this.validateInvalidTextInputContains(this.dtpPolicyEndDateFromSearchFilter, PolicyEndDateFrom, 'Policy end date from'))) {
                logWarningMessage(`PolicyEndDateFrom: Should be shows validation error message when input incorrect format!. The input value: ${PolicyEndDateFrom}`);
                return false;
            }

            if (!(await this.validateInvalidTextInputContains(this.dtpPolicyEndDateToSearchFilter, PolicyEndDateTo, 'Policy end date to'))) {
                logWarningMessage(`PolicyEndDateFrom: Should be shows validation error message when input incorrect format!. The input value: ${PolicyEndDateTo}`);
                return false;
            }

            return true;
        } catch (error) {
            console.log(error);
            return false;
        }
    }

    public async openDetailAccountByName(selectedAccount: string) {
        try {
            //await this.reloadAccountList();  
            await this.driverService.waitUntilElementLoaded(By.xpath("(//app-account-name-column//a)[1]"));
            for (let i = 1; i <= 30; i++) {
                let lblName = By.xpath(`(//app-account-name-column//a)[${i}]`);
                if ((await this.driverService.isExisted(lblName)) === false) {
                    // logWarningMessage(
                    //   `Can't find account with name \"${selectedAccount}\" into Account List`
                    // );
                } else {
                    let nameAccount = await this.driverService.getText(lblName);
                    if (nameAccount.localeCompare(selectedAccount) === 0) {
                        let detailAcc = By.xpath(`(//app-account-name-column//a)[${i}]`);
                        await this.driverService.click(detailAcc);
                        return true;
                    }
                }
            }
            logWarningMessage(`Can't find account with name \"${selectedAccount}\" into Account List`);
            return false;
        } catch (error) {
            log("openDetailAccountByName");
            console.log(error);
            return false;
        }
    }

    public async clickAccountbtn() {
        try {
            await this.driverService.waitForElementEnabled(this.accountbtn);
            await this.driverService.click(this.accountbtn);
        } catch (error) {
            console.log(error);
        }
    }

    //========== Private functions ==========
    private async fillDataIfHasValue(controlElement: By, text: string) {
        if (text) {
            await this.driverService.setText(controlElement, text);
        }
    }

    private getValidationMessageFor(id: string): By {
        return By.xpath(`//label[@for='${id}']/following-sibling::div/formly-validation-message`);
    }

    private getValidationMessageContains(text: string): By {
        return By.xpath(`//label[contains(text(),'${text}')]/following-sibling::div/formly-validation-message`);
    }

    private async validateTextInputFor(
        controlElement: By,
        inputText: string,
        messageLabelId: string,
    ): Promise<boolean> {
        if (!inputText) {   // only validate input when text has value, otherwise not check input format
            return true;
        }

        await this.driverService.setText(controlElement, inputText);
        let validationMessage = this.getValidationMessageFor(messageLabelId);
        const errorMessage = await this.driverService.isExisted(validationMessage);

        return !errorMessage; // return true if there is no error message
    }

    private async validateTextInputContains(
        controlElement: By, 
        inputText: string, 
        messageLabelText: string,
    ): Promise<boolean> {
        if (!inputText) {   // only validate input when text has value, otherwise not check input format
            return true;
        }

        await this.driverService.setText(controlElement, inputText);
        let validationMessage = this.getValidationMessageContains(messageLabelText);
        const errorMessage = await this.driverService.isExisted(validationMessage);

        return !errorMessage; // return true if there is no error message
    }

    private async selectDropdownOptionWaitUntilHorizontalProgressBarLoaded_v2(
        controlElement: By,
        inputText: string,
        lblNoItemsFound: By,
    ): Promise<boolean> {
        if (!inputText) {   // only validate input when text has value, otherwise not check input format
            return true;
        }

        await this.driverService.setText(controlElement, inputText);
        await waitUntilHorizontalProgressBarLoaded_v2(this.driverService);
        const noItemFoundError = await this.driverService.isExisted(lblNoItemsFound);
        if (!noItemFoundError) {
            await selectDropdownOption(inputText, "", this.driverService); // select dropdown if found item
            return true;
        }

        return false; // there is no item found
    }

    private async validateInvalidTextInputFor(
        controlElement: By,
        inputText: string,
        messageLabelId: string,
    ): Promise<boolean> {
        if (!inputText) {   // only validate input when text has value, otherwise not check input format
            return true;
        }

        await this.driverService.setText(controlElement, inputText);
        await this.driverService.pressTabCurrentElement();
        let validationMessage = this.getValidationMessageFor(messageLabelId);
        const errorMessage = await this.driverService.isExisted(validationMessage);

        return errorMessage; // This test should show error message because the input text is invalid, it's opposite with validateTextInputFor()
    }

    private async validateInvalidTextInputContains(
        controlElement: By, 
        inputText: string, 
        messageLabelText: string,
    ): Promise<boolean> {
        if (!inputText) {   // only validate input when text has value, otherwise not check input format
            return true;
        }

        await this.driverService.setText(controlElement, inputText);
        await this.driverService.pressTabCurrentElement();
        let validationMessage = this.getValidationMessageContains(messageLabelText);
        const errorMessage = await this.driverService.isExisted(validationMessage);

        return errorMessage; // This test should show error message because the input text is invalid, it's opposite with validateTextInputFor()
    }
    //=======================================
}
