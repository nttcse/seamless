import { By, Key } from "selenium-webdriver";
import { SeleniumWebDriverService } from "../core/selenium-webdriver.service";
import { fail, strictEqual, notStrictEqual } from "assert";
import { log } from "console";
import { scenarioName, toastError } from "../shared/variables";
import { GlobalPageObject } from "./global-page-object";
import {
    reloadTable,
    waitUntilHorizontalProgressBarLoaded,
    waitUntilHorizontalProgressBarLoaded_v2,
    logWarningMessage,
    // logSuccessMessage,
    // closeToastMessage,
    // selectDropdownOption,
    // logInfoMessage,
} from "../shared/functions";
import { splitText } from "./commons";

export class FrameAgreementPage {
    constructor(private driverService: SeleniumWebDriverService) { }
    private globalPageObject = new GlobalPageObject(this.driverService);

    //Elements
    private btnFrameAgreementsList = By.xpath(
        "//*[@id='pgs-acc-tab-Frame agreements']"
    );
    private btnCreateNewFrameAgreement = By.xpath(
        "//*[@id='create-frame-agreements-dropdown']"
    );

    private btnHistoryList = By.xpath(
        "//*[@id='pgs-acc-tab-History']"
    );

    //Elemments for creating frame agreement
    private txtName = By.xpath(
        "//app-customer-frame-agreement-form//div[./label[text()=' Name ']]//input"
    );
    private txtTotalLimitExposure = By.xpath(
        "//app-customer-frame-agreement-form//div[./label[text()=' Total limit exposure ']]//input"
    );
    private dtpStartDate = By.xpath(
        "//app-customer-frame-agreement-form//div[./label[text()=' Start date ']]//input"
    );
    private dtpEndDate = By.xpath(
        "//app-customer-frame-agreement-form//div[./label[text()=' End date ']]//input"
    );
    private cmbProduct = By.xpath(
        "//app-customer-frame-agreement-form//div[./label[text()=' Product name ']]//input"
    );

    //Element for checking frame agreement details
    private txtRemainingCapacity = By.xpath(
        "//app-customer-frame-agreement-form//div[./label[text()=' Remaining capacity ']]//input"
    );
    private lblProduct = By.xpath(
        "//app-customer-frame-agreement-product-field//div//table//tr[1]//td[2]/span"
    );
    private lblPremiumRate = By.xpath(
        "//app-customer-frame-agreement-product-field//div//table//tr[1]//td[3]//div/span[1]"
    );
    private lblCommissionRate = By.xpath(
        "//app-customer-frame-agreement-product-field//div//table//tr[1]//td[3]//div/span[2]"
    );
    private lblPaymentMethod = By.xpath(
        "//app-customer-frame-agreement-product-field//div//table//tr[1]//td[3]//div/span[3]"
    );
    private lblGuaranteeRate = By.xpath(
        "//app-customer-frame-agreement-product-field//div//table//tr[1]//td[3]//div/span[4]"
    );
    private lblEstablishmentFee = By.xpath(
        "//app-customer-frame-agreement-product-field//div//table//tr[1]//td[3]//div/span[5]"
    );
    private lblAmendmentFee = By.xpath(
        "//app-customer-frame-agreement-product-field//div//table//tr[1]//td[3]//div/span[6]"
    );

    private lblFirstPhaseGuaranteeRate = By.xpath(
        "//app-customer-frame-agreement-product-field//div//table//tr[1]//td[3]//div/span[4]"
    );

    private lblSecondPhaseGuaranteeRate = By.xpath(
        "//app-customer-frame-agreement-product-field//div//table//tr[1]//td[3]//div/span[5]"
    );

    private lblEstablishmentFee2 = By.xpath(
        "//app-customer-frame-agreement-product-field//div//table//tr[1]//td[3]//div/span[6]"
    );
    private lblAmendmentFee2 = By.xpath(
        "//app-customer-frame-agreement-product-field//div//table//tr[1]//td[3]//div/span[7]"
    );

    //Elements for adding product
    private txtPremiumRate = By.xpath(
        "//app-customer-frame-agreement-product-form//div[./label[text()=' Premium Rate (%) ']]//input"
    );
    private txtCommissionRate = By.xpath(
        "//app-customer-frame-agreement-product-form//div[./label[text()=' Commission Rate (%) ']]//input"
    );
    private cmbPaymentMethod = By.xpath(
        "//app-customer-frame-agreement-product-form//div[./label[text()=' Payment method ']]//input"
    );
    private txtGuaranteeRate = By.xpath(
        "//app-customer-frame-agreement-product-form//div[./label[text()=' Guarantee Rate (%) ']]//input"
    );

    private txtFirstPhaseGuaranteeRate = By.xpath(
        "//app-customer-frame-agreement-product-form//div[./label[text()=' First Phase Guarantee Rate (%) ']]//input"
    );

    private txtSecondPhaseGuaranteeRate = By.xpath(
        "//app-customer-frame-agreement-product-form//div[./label[text()=' Second Phase Guarantee Rate (%) ']]//input"
    );

    private txtEstablishmentFee = By.xpath(
        "//app-customer-frame-agreement-product-form//div[./label[text()=' Establishment Fee ']]//input"
    );
    private txtAmendmentFee = By.xpath(
        "//app-customer-frame-agreement-product-form//div[./label[text()=' Amendment Fee ']]//input"
    );

    public async checkAccountOpening(): Promise<boolean> {
        let accountDetail = By.xpath(
            "//app-customer-page//div[contains(@class,'active') and contains(@class,'tab-pane')]//app-customer-detail"
        );
        return await this.driverService.isExisted(accountDetail);
    }

    //Go to Frame Agreements List
    public async navigateToFrameAgreementsList() {
        try {
            await this.driverService.waitUntilElementLoaded(
                this.btnFrameAgreementsList
            );
            await waitUntilHorizontalProgressBarLoaded_v2(this.driverService, 500);
            await this.driverService.click(this.btnFrameAgreementsList);
            return true;
        } catch (error) {
            console.log("navigateToFrameAgreementList");
            console.log(error);
            return false;
        }
    }

    public async openCreateNewFrameAgreementForm() {
        try {
            await this.driverService.waitUntilElementLoaded(
                this.btnCreateNewFrameAgreement
            );
            await this.driverService.waitForSeconds(500);
            await this.driverService.click(this.btnCreateNewFrameAgreement);
            return true;
        } catch (error) {
            console.log("Open Create New Frame Agreement form");
            console.log(error);
            return false;
        }
    }

    public async inputDataToCreateFrameAgreement(
        Name: string,
        TotalLimitExposure: string,
        StartDate: string,
        EndDate: string,
        Product: string
    ) {
        try {
            await this.driverService.waitUntilElementLoaded(this.txtName);

            //input new data from file
            await this.driverService.setText(this.txtName, Name);
            await this.driverService.setText(
                this.txtTotalLimitExposure,
                TotalLimitExposure
            );
            await this.driverService.setText(this.dtpStartDate, StartDate);
            await this.driverService.setText(this.dtpEndDate, EndDate);
            await this.driverService.setText(this.cmbProduct, Product);
            await this.driverService.pressEnterCurrentElement();

            return true;
        } catch (error) {
            console.log("Input data to Create Frame Agreement");
            logWarningMessage("Input data to Create Frame Agreement: failed!");
            console.log(error);
            return false;
        }
    }

    public async inputDataToAddProduct(
        Type: string,
        PremiumRate: string,
        CommissionRate: string,
        PaymentMethod: string,
        GuaranteeRate: string,
        FirstPhaseGuaranteeRate: string,
        SecondPhaseGuaranteeRate: string,
        EstablishmentFee: string,
        AmendmentFee: string
    ) {
        try {
            await this.driverService.waitUntilElementLoaded(this.txtPremiumRate);
            await this.driverService.waitUntilElementLoaded(this.txtCommissionRate);
            await this.driverService.waitUntilElementLoaded(this.cmbPaymentMethod);
            await this.driverService.waitUntilElementLoaded(
                this.txtEstablishmentFee
            );
            await this.driverService.waitUntilElementLoaded(this.txtAmendmentFee);

            //input new data from file
            await this.driverService.setText(this.txtPremiumRate, PremiumRate);
            await this.driverService.setText(this.txtCommissionRate, CommissionRate);
            await this.driverService.setText(this.cmbPaymentMethod, PaymentMethod);
            await this.driverService.pressEnterCurrentElement();
            if (Type == 'One Phase') {
                await this.driverService.setText(this.txtGuaranteeRate, GuaranteeRate);
            }
            if (Type == 'Two Phases') {
                await this.driverService.setText(this.txtFirstPhaseGuaranteeRate, FirstPhaseGuaranteeRate);
                await this.driverService.setText(this.txtSecondPhaseGuaranteeRate, SecondPhaseGuaranteeRate);
            }
            await this.driverService.setText(this.txtEstablishmentFee, EstablishmentFee);
            await this.driverService.setText(this.txtAmendmentFee, AmendmentFee);

            return true;
        } catch (error) {
            console.log("Input data to Create Frame Agreement");
            logWarningMessage("Input data to Create Frame Agreement: failed!");
            console.log(error);
            return false;
        }
    }

    public async reloadFrameAgreementList() {
        try {
            await this.driverService.waitForSeconds(10000);
            await reloadTable(this.driverService);
            await this.driverService.waitForSeconds(2000);
            await reloadTable(this.driverService);
        } catch (error) {
            console.log(error);
        }
    }

    public async openHistoryList() {
        try {
            await this.driverService.waitUntilElementLoaded(
                this.btnHistoryList
            );
            await this.driverService.waitForSeconds(500);
            await this.driverService.click(this.btnHistoryList);
            return true;
        } catch (error) {
            console.log("Open History List");
            console.log(error);
            return false;
        }
    }

    public async assertCreateFrameAgreement(
        //positionRow: number,
        name: string,
        totallimitexposure: string,
        startdate: string,
        enddate: string,
        actualcapacity: string,
        actualremainingcapacity: string,
        actualstatus: string
    ) {
        let actualName: string = "";
        let actualTotalLimitExposure: string = "";
        let actualPeriod: string = "";
        let actualStartDate: string = "";
        let actualEndDate: string = "";
        let actualCapacity: string = "";
        let actualRemainingCapacity: string = "";
        let actualStatus: string = "";

        const splitDate = (date) => {
            const arr = date.split(" - ");
            return { leftDate: arr[0].trim(), rightDate: arr[1].trim() };
        };

        try {
            //Locator for elements at row {positionRow} of frame agreement list
            let lblName = By.xpath(
                "//div[contains(@class,'tab-pane') and contains(@class,'active')]//table//tr[1]//td[3]//*[self::*[text()]]"
            );
            let lblTotalLimitExposure = By.xpath(
                "//div[contains(@class,'tab-pane') and contains(@class,'active')]//table//tr[1]//td[4]//*[self::*[text()]]"
            );
            let lblPeriod = By.xpath(
                "//div[contains(@class,'tab-pane') and contains(@class,'active')]//table//tr[1]//td[2]//span[@title]"
            );
            let lblCapacity = By.xpath(
                "//div[contains(@class,'tab-pane') and contains(@class,'active')]//table//tr[1]//td[5]//*[self::*[text()]]"
            );

            let lblRemainingCapacity = By.xpath(
                "//div[contains(@class,'tab-pane') and contains(@class,'active')]//table//tr[1]//td[6]//*[self::*[text()]]"
            );

            let lblStatus = By.xpath(
                "//div[contains(@class,'tab-pane') and contains(@class,'active')]//table//tr[1]//td[7]//*[self::*[text()]]"
            );

            await this.driverService.waitUntilElementLoaded(lblName);
            await this.driverService.waitForSeconds(10000);
            actualName = await (
                await this.driverService.findElement(lblName)
            ).getText();

            actualTotalLimitExposure = await (
                await this.driverService.findElement(lblTotalLimitExposure)
            ).getText();

            actualPeriod = await (
                await this.driverService.findElement(lblPeriod)
            ).getText();

            const temp = splitDate(actualPeriod);
            actualStartDate = temp.leftDate;
            actualEndDate = temp.rightDate;

            actualCapacity = await (
                await this.driverService.findElement(lblCapacity)
            ).getText();

            actualRemainingCapacity = await (
                await this.driverService.findElement(lblRemainingCapacity)
            ).getText();

            actualStatus = await (
                await this.driverService.findElement(lblStatus)
            ).getText();

        } catch (error) {
            console.log("Assert Create Frame Agreement");
            console.log(error);
        }

        await this.driverService.validateTestCase(
            "Create frame agreement successfully!",
            [actualName, name, "Assert at Name: Incorrect Name!"],
            [
                actualTotalLimitExposure,
                totallimitexposure,
                "Assert at Total Limit Exposure: Incorrect Total Limit Exposure!",
            ],
            [
                actualStartDate,
                startdate,
                "Assert at Start Date: Incorrect Start Date!",
            ],
            [
                actualEndDate,
                enddate,
                "Assert at End Date: Incorrect End Date!",
            ],
            [
                actualCapacity,
                actualcapacity,
                "Assert at Capacity: Incorrect Capacity!",
            ],
            [
                actualRemainingCapacity,
                actualremainingcapacity,
                "Assert at Remaining Capacity: Incorrect Remaining Capacity!",
            ],
            [
                actualStatus,
                actualstatus,
                "Assert at Status: Incorrect Status!",
            ]
        );
    }

    public async getFrameAgreementNo() {
        let lblFrameAgreementNo = By.xpath("//div[contains(@class,'tab-pane') and contains(@class,'active')]//table//tr[1]//td[2]//a[@title]");

        let actualFrameAgreementNo: string = "";

        actualFrameAgreementNo = await (
            await this.driverService.findElement(lblFrameAgreementNo)
        ).getText();

        return actualFrameAgreementNo;
    }

    public async assertHistory(
        type: string,
        description: string,
        timestamp: string
    ) {
        let actualType: string = "";
        let actualDescription: string = "";
        let actualTime: string = "";
        let actualDate: string = "";

        const splitTime = (date) => {
            const arr = date.split(" ");
            return { leftDate: arr[0].trim(), rightHour: arr[1].trim() };
        };

        try {
            //Locator for elements at row {positionRow} of history list
            let lblType = By.xpath("//div[contains(@class,'tab-pane') and contains(@class,'active')]//table//tr[1]//td[1]//*[self::*[text()]]");
            let lblDescription = By.xpath("//div[contains(@class,'tab-pane') and contains(@class,'active')]//table//tr[1]//td[2]//app-event-description/a[@href]");
            let lblTime = By.xpath("//div[contains(@class,'tab-pane') and contains(@class,'active')]//div[contains(@class,'tab-pane') and contains(@class,'active')]//tr[1]//td[3]/*[text()]");

            await this.driverService.waitUntilElementLoaded(lblType);
            await this.driverService.waitForSeconds(10000);

            actualType = await (
                await this.driverService.findElement(lblType)
            ).getText();

            actualDescription = await (
                await this.driverService.findElement(lblDescription)
            ).getText();

            actualTime = await (
                await this.driverService.findElement(lblTime)
            ).getText();

            const temp = splitTime(actualTime);
            actualDate = temp.leftDate;

        } catch (error) {
            console.log("Assert History");
            console.log(error);
        }

        await this.driverService.validateTestCase(
            "Create frame agreement successfully!",
            [
                actualType,
                type,
                "Assert at Type: Incorrect Type!",
            ],
            [
                actualDescription,
                description,
                "Assert at Description: Incorrect Description!",
            ],
            [
                actualDate,
                timestamp,
                "Assert at Time: Incorrect Time Stamp!",
            ]
        );
    }

    public async openFrameAgreementDetails(selectedFrameAgreement: string) {
        try {
            await this.driverService.waitUntilElementLoaded(By.xpath("//div[contains(@class,'tab-pane') and contains(@class,'active')]//table//tr[1]//td[2]//a"));
            for (let i = 1; i <= 30; i++) {
                let lblFrameAgreementNo = By.xpath(`//div[contains(@class,'tab-pane') and contains(@class,'active')]//table//tr[1]//td[2]//a[${i}]`);
                if ((await this.driverService.isExisted(lblFrameAgreementNo)) === false) {

                } else {
                    let nameFrameAgreement = await this.driverService.getText(lblFrameAgreementNo);
                    //console.log("JOLIE TEST", nameFrameAgreement);
                    if (nameFrameAgreement.localeCompare(selectedFrameAgreement) === 0) {
                        let detailFA = By.xpath(`//div[contains(@class,'tab-pane') and contains(@class,'active')]//table//tr[1]//td[2]//a[${i}]`);
                        await this.driverService.click(detailFA);
                        return true;
                    }
                }
            }
            logWarningMessage(`Can't find frame agreement with name \"${selectedFrameAgreement}\" into Frame Agreement List`);
            return false;
        } catch (error) {
            log("open frame agreement details");
            console.log(error);
            return false;
        }
    }

    public async assertFrameAgreementDetails(
        //positionRow: number,
        type: string,
        name: string,
        totallimitexposure: string,
        startdate: string,
        enddate: string,
        remainingcapacity: string,
        product: string,
        premiumrate: string,
        commissionrate: string,
        paymentmethod: string,
        guaranteerate: string,
        firstphaseguaranteerate: string,
        secondphaseguaranteerate: string,
        establishmentfee: string,
        amendmentfee: string
    ) {
        let actualName: string = "";
        let actualTotalLimitExposure: string = "";
        let actualStartDate: string = "";
        let actualEndDate: string = "";
        let actualRemainingCapacity: string = "";
        let actualProduct: string = "";
        let actualPremiumRate: string = "";
        let actualPremiumRateTmp: string = "";
        let actualCommissionRate: string = "";
        let actualCommissionRateTmp: string = "";
        let actualPaymentMethod: string = "";
        let actualPaymentMethodTmp: string = "";
        let actualGuaranteeRateTmp: string = "";
        let actualGuaranteeRate: string = "";
        let actualFirstPhaseGuaranteeRateTmp: string = "";
        let actualFirstPhaseGuaranteeRate: string = "";
        let actualSecondPhaseGuaranteeRateTmp: string = "";
        let actualSecondPhaseGuaranteeRate: string = "";
        let actualEstablishmentFee: string = "";
        let actualEstablishmentFeeTmp: string = "";
        let actualAmendmentFee: string = "";
        let actualAmendmentFeeTmp: string = "";
        let actualEstablishmentFee2: string = "";
        let actualEstablishmentFee2Tmp: string = "";
        let actualAmendmentFee2: string = "";
        let actualAmendmentFee2Tmp: string = "";


        try {
            await this.driverService.waitUntilElementLoaded(this.txtName);
            await this.driverService.waitForSeconds(10000);

            actualName = await this.driverService.getAttributeValue(this.txtName, 'value');
            actualTotalLimitExposure = await this.driverService.getAttributeValue(this.txtTotalLimitExposure, 'value');
            actualStartDate = await this.driverService.getAttributeValue(this.dtpStartDate, 'value');
            actualEndDate = await this.driverService.getAttributeValue(this.dtpEndDate, 'value');
            actualRemainingCapacity = await this.driverService.getAttributeValue(this.txtRemainingCapacity, 'value');

            actualProduct = await (
                await this.driverService.findElement(this.lblProduct)
            ).getText();

            actualPremiumRateTmp = await (
                await this.driverService.findElement(this.lblPremiumRate)
            ).getText();

            const temp = splitText(actualPremiumRateTmp);
            actualPremiumRate = temp.rightText;

            actualCommissionRateTmp = await (
                await this.driverService.findElement(this.lblCommissionRate)
            ).getText();

            actualCommissionRate = splitText(actualCommissionRateTmp).rightText

            actualPaymentMethodTmp = await (
                await this.driverService.findElement(this.lblPaymentMethod)
            ).getText();

            actualPaymentMethod = splitText(actualPaymentMethodTmp).rightText;

            if (type == 'One Phase') {
                actualGuaranteeRateTmp = await (
                    await this.driverService.findElement(this.lblGuaranteeRate)
                ).getText();

                actualGuaranteeRate = splitText(actualGuaranteeRateTmp).rightText;

                actualEstablishmentFeeTmp = await (
                    await this.driverService.findElement(this.lblEstablishmentFee)
                ).getText();

                actualEstablishmentFee = splitText(actualEstablishmentFeeTmp).rightText;

                actualAmendmentFeeTmp = await (
                    await this.driverService.findElement(this.lblAmendmentFee)
                ).getText();

                actualAmendmentFee = splitText(actualAmendmentFeeTmp).rightText;
            }
            if (type == 'Two Phases') {
                actualFirstPhaseGuaranteeRateTmp = await (
                    await this.driverService.findElement(this.lblFirstPhaseGuaranteeRate)
                ).getText();

                actualFirstPhaseGuaranteeRate = splitText(actualFirstPhaseGuaranteeRateTmp).rightText;

                actualSecondPhaseGuaranteeRateTmp = await (
                    await this.driverService.findElement(this.lblSecondPhaseGuaranteeRate)
                ).getText();

                actualFirstPhaseGuaranteeRate = splitText(actualFirstPhaseGuaranteeRateTmp).rightText;

                actualEstablishmentFee2Tmp = await (
                    await this.driverService.findElement(this.lblEstablishmentFee2)
                ).getText();

                actualEstablishmentFee2 = splitText(actualEstablishmentFee2Tmp).rightText;

                actualAmendmentFee2Tmp = await (
                    await this.driverService.findElement(this.lblAmendmentFee2)
                ).getText();

                actualAmendmentFee2 = splitText(actualAmendmentFee2Tmp).rightText;
            }

        } catch (error) {
            console.log("Assert Create Frame Agreement");
            console.log(error);
        }

        await this.driverService.validateTestCase(
            "Create frame agreement successfully!",
            [actualName, name, "Assert at Name: Incorrect Name!"],
            [
                actualTotalLimitExposure,
                totallimitexposure,
                "Assert at Total Limit Exposure: Incorrect Total Limit Exposure!",
            ],
            [
                actualStartDate,
                startdate,
                "Assert at Start Date: Incorrect Start Date!",
            ],
            [
                actualEndDate,
                enddate,
                "Assert at End Date: Incorrect End Date!",
            ],
            [
                actualRemainingCapacity,
                remainingcapacity,
                "Assert at Remaining Capacity: Incorrect Remaining Capacity!",
            ],
            [
                actualProduct,
                product,
                "Assert at Status: Incorrect Product!",
            ],
            [
                actualPremiumRate,
                premiumrate,
                "Assert at Status: Incorrect Premium Rate!",
            ],
            [
                actualCommissionRate,
                commissionrate,
                "Assert at Status: Incorrect Commission Rate!",
            ],
            [
                actualPaymentMethod,
                paymentmethod,
                "Assert at Status: Incorrect Payment Method!",
            ],
            [
                actualGuaranteeRate,
                guaranteerate,
                "Assert at Status: Incorrect Payment Method!",
            ],
            [
                actualEstablishmentFee,
                establishmentfee,
                "Assert at Status: Incorrect Actual Establishment Fee!",
            ],
            [
                actualAmendmentFee,
                amendmentfee,
                "Assert at Status: Incorrect Actual Amendment Fee!",
            ]
        );
    }
}
