import { SeleniumWebDriverService } from '../core/selenium-webdriver.service';
import { By, Locator } from 'selenium-webdriver';
import { waitForElementTimeOut } from './constants';
import { pushSubErrorMessage, scenarioName, subErrorMessages, toastSuccess, toastError, locator_progressbarActive, locator_progressbarNotActive } from './variables';
import fs from 'fs';
import { fail } from 'assert';
export async function waitUntilHorizontalProgressBarLoaded(driverService: SeleniumWebDriverService, timeOut: number = waitForElementTimeOut) {
    await driverService.waitUntilElementLoaded(locator_progressbarActive, timeOut);
    await driverService.waitUntilElementLoaded(locator_progressbarNotActive, timeOut);
}

export async function waitUntilHorizontalProgressBarLoaded_v2(driverService: SeleniumWebDriverService, millisecondNumber: number = 1000, timeOut: number = waitForElementTimeOut) {
    await driverService.waitForSeconds(millisecondNumber);
    await driverService.waitUntilElementLoaded(locator_progressbarNotActive, timeOut);
}

export async function reloadTable(driverService: SeleniumWebDriverService) {
    let reloadList = By.xpath(`(//*[contains(@class,'pagination')]//i[contains(@class,'fal') and contains(@class,'fa-sync')])`);
    let len = (await driverService.findElements(reloadList)).length;
    if (await driverService.isExisted(toastSuccess)) {
        try {
            await driverService.click(toastSuccess);
        } catch (error) {
            // do nothing
        }
    }
    if (await driverService.isExisted(toastError)) {
        try {
            await driverService.click(toastError);
        } catch (error) {
            // do nothing
        }
    }
    for (let i = 1; i <= len; i++) {
        try {
            reloadList = By.xpath(`(//*[contains(@class,'pagination')]//i[contains(@class,'fal') and contains(@class,'fa-sync')])[${i}]`);
            await driverService.click(reloadList);
            await waitUntilHorizontalProgressBarLoaded(driverService);
            await driverService.pressPageUpCurrentElement();
            return;
        } catch (error) {
            continue;
        }
    }
}

async function pressPageUpCurrentList(driverService: SeleniumWebDriverService) {
    let table = By.xpath("//table/thead");
    let len = (await driverService.findElements(table)).length;
    for (let i = 1; i <= len; i++) {
        table = By.xpath(`(//table/thead)[${i}]`);
        try {
            await driverService.click(table);
            await driverService.pressPageUpCurrentElement();
            return;
        } catch (error) {
            continue;
        }
    }
}

export async function expandNumberItemOfList(driverService: SeleniumWebDriverService, locatorItemPage: Locator = By.xpath("//button[@id='pgs-expand-rows-btn']"), numberItem: number = 30) {
    await driverService.click(locatorItemPage);
    if (numberItem == 20) {
        await driverService.pressTabCurrentElement();
        await driverService.pressTabCurrentElement();
    }
    if (numberItem == 30) {
        await driverService.pressTabCurrentElement();
        await driverService.pressTabCurrentElement();
        await driverService.pressTabCurrentElement();
    }
    await driverService.pressEnterCurrentElement();
    await waitUntilHorizontalProgressBarLoaded(driverService);
    await driverService.pressTabCurrentElement();
    await pressPageUpCurrentList(driverService);
}


//check validation for SSN/Org
export function validateKontonummerMod11(kontonummer: string): boolean {
    const weights = [5, 4, 3, 2, 7, 6, 5, 4, 3, 2];
    const kontonummerWithoutSpacesAndPeriods = kontonummer.replace(/[\s.]+/g, '');
    if (kontonummerWithoutSpacesAndPeriods.length !== 11) {
        return false;
    } else {
        const sjekksiffer = parseInt(kontonummerWithoutSpacesAndPeriods.charAt(10), 10);
        const kontonummerUtenSjekksiffer = kontonummerWithoutSpacesAndPeriods.substring(0, 10);
        let sum = 0;
        for (let index = 0; index < 10; index++) {
            sum += parseInt(kontonummerUtenSjekksiffer.charAt(index), 10) * weights[index];
        }
        const remainder = sum % 11;
        return sjekksiffer === (remainder === 0 ? 0 : 11 - remainder);
    }
}

// return random number of SSN or Org. No
export function randomModulus11ForSSN() {
    let result = "";
    while (1) {
        const number = Math.random().toString().substring(2, 12); //get random a number with length = 10
        for (let i = 0; i <= 9; i++) {
            let ssn = number + i.toString();
            if (validateKontonummerMod11(ssn)) {
                return ssn;
            }
        }
    }
    return result;
}

//Get current Datetime with format: dd/mm/yyyy hh: MM
export function getCurrentDateTime(): string {
    let today = new Date();
    let dd = String(today.getDate()).padStart(2, "0");
    let mm = String(today.getMonth() + 1).padStart(2, "0"); //January is 0!
    let yyyy = today.getFullYear();
    let hh: string = today.getHours().toString();
    if (hh.length === 1) {
        hh = "0" + hh;
    }

    let MM: string = today.getMinutes().toString();
    if (MM.length === 1) {
        MM = "0" + MM;
    }
    let result: string = dd + "/" + mm + "/" + yyyy + " " + hh + ":" + MM;
    return result;
}

//Log message to console with color
export function logSuccessMessage(message: string) {
    console.log("\x1b[32m",// green foreground color
        message,
        "\x1b[0m");
}
export function logInfoMessage(message: string) {
    console.info("\x1b[36m", message, "\x1b[0m"); //Info message (Cyan foreground color)
}
export function logFailMessage(message: string) {
    console.log("\x1b[31m",// red foreground color
        message,
        "\x1b[0m");
}

export function logFailTestcase(isPassed: boolean, message: string = "") {
    if (!isPassed) {
        if (message) {
            logWarningMessage(message);
        }
        fail(scenarioName + ": is failed!" + subErrorMessages);
    }
}

export function logWarningMessage(message: string) {
    console.log("\x1b[33m%s\x1b[0m", message, "\x1b[0m");//yellow foreground color
    pushSubErrorMessage(message);
}

export function getLineInFileTxt(fileName: string, lineNumber: number) {
    const data = fs.readFileSync(fileName, { encoding: 'utf8', flag: 'r' });
    const line = data.split('\n');
    return line[lineNumber];
}

export function readJsonFileToObject(fileName: string) {
    return JSON.parse(fs.readFileSync(fileName, 'utf8'));
}

export async function selectDropdownOption(optionName: string, rootXpath: string = "", driverService: SeleniumWebDriverService) {
    await driverService.waitForSeconds(100);
    await driverService.pressDownCurrentElement();
    let option = By.xpath(`${rootXpath}//ng-dropdown-panel//div/*[self::*[text()='${optionName}'] or self::*[text()=' ${optionName}'] or self::*[text()='${optionName} '] or self::*[text()=' ${optionName} ']]`);
    await driverService.click(option);
}

export async function closeToastMessage(driverService: SeleniumWebDriverService) {
    while (await driverService.isExisted(toastError)) {
        await driverService.click(toastError);
        await driverService.waitForSeconds(500);
    }
    while (await driverService.isExisted(toastSuccess)) {
        await driverService.click(toastSuccess);
        await driverService.waitForSeconds(500);
    }
}

/**
 * 
 * @param stage 100% - Won
 * @returns Won - 100%
 * 
 * or
 * @param stage Won - 100%
 * @returns 100% - Won
 */
export function reformatSalesStage(stage: string): string {
    let result = stage.split(" - ");
    return result[1] + " - " + result[0];
}