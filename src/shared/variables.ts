import { ValidateField } from './classes';
import { Locator, By } from 'selenium-webdriver';
export var dataTestcase: ValidateField[] = [];
export function resetDataTestcase() {
    dataTestcase = [];
}
export const toastError: Locator = By.xpath("//div[@id='toast-container']/div[contains(@class,'toast-error')]/div");
export const toastSuccess: Locator = By.xpath("//div[@id='toast-container']/div[contains(@class,'toast-success')]/div");

export const locator_progressbarActive: Locator = By.xpath("//ng-progress/div[contains(@class,'ng-progress-bar') and @active='true'] | //ng-progress/div[contains(@class,'ng-progress-bar -active')]");
export const locator_progressbarNotActive: Locator = By.xpath("//ng-progress/div[contains(@class,'ng-progress-bar') and @active='false'] | //ng-progress/div[not(@active) and @class='ng-progress-bar']");

//Scenario name
export let scenarioName = "";
export function setScenarioName(name: string) {
    scenarioName = name;
}

//Sub error messages
export let subErrorMessages = "\n\tSub error messages: ";
export function pushSubErrorMessage(errorMessage: string) {
    subErrorMessages = subErrorMessages + "\n\t\t- " + errorMessage;
}
export function resetSubErrorMessage() {
    subErrorMessages = "\n\tSub error messages: ";
}