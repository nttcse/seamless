import {
    After,
    AfterAll,
    Before,
    BeforeAll,
    setDefaultTimeout,
    Status,
    World,
} from "@cucumber/cucumber";
import { GlobalPageObject } from "../../page-objects/global-page-object";
import { WebDriver } from "selenium-webdriver";
import { defaultTimeOut } from "../../shared/constants";
import * as logger from "../logger.service";
import { SeleniumWebDriverService } from "../selenium-webdriver.service";
import { Utilities } from "../utilities";
import { resetSubErrorMessage, setScenarioName, resetDataTestcase } from '../../shared/variables';

let driver: WebDriver;
let driverService: SeleniumWebDriverService;
let globalPageObject: GlobalPageObject;
setDefaultTimeout(defaultTimeOut);

BeforeAll(async () => {

    driver = Utilities.createSeleniumDriverSession();
    driverService = new SeleniumWebDriverService(driver);
    globalPageObject = new GlobalPageObject(driverService);
    await driverService.maximizeWindow();
});

// tslint:disable-next-line: only-arrow-functions
// Before({tags: '@ignore'}, async function() {
//     return 'skipped';
// });

Before(async function (scenario) {
    driver.manage().deleteAllCookies();
    this.context = {
        ...this.context,
        driver,
        driverService,
    };
    setScenarioName(scenario.pickle.name || '');
    resetSubErrorMessage();
    resetDataTestcase();
});

//This Hook use for Smoke test, browser will logout after every Testcase
After({ tags: "not @RegressionTest" }, async function (this: World, hookForResult: any) {
    const scenarioResult = hookForResult.result.status;
    const scenarioName = hookForResult.pickle.name;
    switch (scenarioResult) {
        case Status.PASSED: {
            logger.info(`Scenario Passed: ${scenarioName}`);
            break;
        }
        case Status.FAILED: {
            logger.error(`Scenario Failed: ${scenarioName}`);

            // Attach screen shot to report
            const buffer = await driver.takeScreenshot();
            this.attach(buffer, "image/png");
            driverService.takeScreenShot(scenarioName);
        }
    }
    //added this to make sure "takeScreenShot the errors" happen before pressed logout
    await globalPageObject.closeOpeningForm();
    //await globalPageObject.navigateToMainLogOut();
});

//This Hook use for Regresstion test, browser will NOT logout after every Testcase
After({ tags: "@RegressionTest" }, async function (this: World, hookForResult: any) {
    const scenarioResult = hookForResult.result.status;
    const scenarioName = hookForResult.pickle.name;
    switch (scenarioResult) {
        case Status.PASSED: {
            logger.info(`Scenario Passed: ${scenarioName}`);
            break;
        }
        case Status.FAILED: {
            logger.error(`Scenario Failed: ${scenarioName}`);

            // Attach screen shot to report
            const buffer = await driver.takeScreenshot();
            this.attach(buffer, "image/png");
            driverService.takeScreenShot(scenarioName);
        }
    }
    await globalPageObject.closeOpeningForm();
});

AfterAll(async () => {
    await driver.quit();
});
