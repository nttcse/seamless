import { Builder } from 'selenium-webdriver';
import { settings } from '../shared/settings';

export class Utilities {
    public static createSeleniumDriverSession() {
        return new Builder()
        // Enable to use remote selenium hub.
            //.usingServer(settings.selenium.serverUrl)
            .withCapabilities(settings.selenium.capabilities)
            .build();
    }
}
